<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    //

    	protected $table = 'tbl_member';

   		protected $fillable = [
        'NIK','user_id','user_register_id','complete_name','gol_darah','jenis_kelamin','tempat_lahir','tanggal_lahir','nama_ayah','nama_ibu','nama_pasangan','status_perkawinan','alamat','provinsi','kabupatenkota','kecamatan','kelurahan','kode_pos','jenis_pekerjaan','nama_kantor','alamat_kantor','telp','email','pendidikan_sd','thn_pendidikan_sd','pendidikan_smp','thn_pendidikan_smp','pendidikan_sma','thn_pendidikan_sma','pendidikan_sarjana','thn_pendidikan_sarjana','pengalaman_kerja1','thn_pengalaman_kerja1','pengalaman_kerja2','thn_pengalaman_kerja2','pengalaman_kerja3','thn_pengalaman_kerja3','pengalaman_kerja4','thn_pengalaman_kerja4','status'];

}
