<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use DB;
use Auth;

class MemberController extends Controller
{
    //
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('member.home');
    }
    public function profil()
    {


        $id = Auth::user()->id;
        $pointer=DB::select("CALL GetMemberCheck(?)",[$id]);
        $member=DB::select("CALL GetMemberProfile(?)",[$id]);

        

        //return response()->json($pointer[0]);
        if($pointer[0]->counter == 1){
            return view('member.profil')->with('member',$member[0])->with('pointer',$pointer[0]->counter);
        }else{
            return view('member.profil')->with('pointer',$pointer[0]->counter);
        }
        
    }
    public function insertProfil()
    {

        return view('member.input_profil');
   
        
    }
    public function updateProfil()
    {


        $id = Auth::user()->id;
        $member=DB::select("CALL GetMemberProfile(?)",[$id]);


        //return response()->json($pointer[0]);

        return view('member.update_profil')->with('user',$member[0]);
   
        
    }
    public function updateProfilProcess(Request $request)
    {

                $messages = [
                    'NIK.required' => 'NIK belum terisi',
                    'complete_name.required' => 'Nama Lengkap belum terisi',
                    'gol_darah.required' => 'Golongan darah belum dipilih',
                    'jenis_kelamin.required' => 'Jenis Kelamin belum dipilih',
                    'tempat_lahir.required' => 'Tempat Lahir belum terisi',
                    'tanggal_lahir.required' => 'Tanggal Lahir belum terisi',
                    'opt_kawin.required' => 'Status perkawinan belum dipilih',
                    'psg.required_if' => ' Nama Pasangan belum terisi',
                    'nama_ayah.required' => 'Nama ayah belum terisi',
                    'nama_ibu.required' => 'Nama ibu belum terisi',
                    'alamat.required' => 'Alamat belum terisi',
                    'prov.required' => 'Provinsi belum terisi',
                    'kk.required' => 'Kabupaten Kota belum terisi',
                    'kc.required' => 'Kecamatan belum terisi',
                    'kl.required' => 'Kelurahan belum terisi',
                    'kpos.required' => 'Kode Pos belum terisi',
                    'telp.required' => 'Telephone belum terisi',
                    'nama_sd.required' => 'Nama Sekolah SD belum terisi',
                    'tahun_sd.required' => 'Tahun Lulus Sekolah SD belum terisi',
                    'nama_smp.required' => 'Nama Sekolah SMP belum terisi',
                    'tahun_smp.required' => 'Tahun Lulus Sekolah SMP belum terisi',
                    'nama_sma.required' => 'Nama Sekolah SMA belum terisi',
                    'tahun_sma.required' => 'Nama Sekolah SMA belum terisi',
                ];

               $rules = [
            'NIK' => 'required',
            'complete_name' => 'required',
            'gol_darah' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'opt_kawin' => 'required',
            'psg' => 'required_if:opt_kawin,1',
            'alamat' => 'required',
            'prov' => 'required',
            'kk' => 'required',
            'kc' => 'required',
            'kl' => 'required',
            'kpos' => 'required',
            'telp' => 'required',
            'nama_sd' => 'required',
            'tahun_sd' => 'required',
            'nama_smp' => 'required',
            'tahun_smp' => 'required',
            'nama_sma' => 'required',
            'tahun_sma' => 'required'
        ];

        //validation
       $request->validate($rules,$messages);

            //to tbl_member
        $NIK =  $request->input('NIK');
        $user_id = Auth::user()->id;
        $user_register_id = $request->input('user_register_id');
        $complete_name = $request->input('complete_name');
        $gol_darah = $request->input('gol_darah');
        $jenis_kelamin = $request->input('jenis_kelamin');
        $tempat_lahir = $request->input('tempat_lahir');
        $tanggal_lahir = $request->input('tanggal_lahir');
        $status_perkawinan = $request->input('opt_kawin');
        $nama_pasangan=$request->input('psg');
        $nama_ayah = $request->input('ayah');
        $nama_ibu = $request->input('ibu');
        $alamat = $request->input('alamat');
        $provinsi = $request->input('prov');
        $kabupatenkota = $request->input('kk');
        $kecamatan = $request->input('kc');
        $kelurahan = $request->input('kl');
        $kode_pos = $request->input('kpos');
        

        $telp = $request->input('telp');
        //$email = Auth::user()->email;
        $pendidikan_sd = $request->input('nama_sd');
        $thn_pendidikan_sd = $request->input('tahun_sd');
        $pendidikan_smp = $request->input('nama_smp');
        $thn_pendidikan_smp = $request->input('tahun_smp');
        $pendidikan_sma = $request->input('nama_sma');
        $thn_pendidikan_sma = $request->input('tahun_sma');
        $pendidikan_sarjana = $request->input('nama_pt');
        $thn_pendidikan_sarjana = $request->input('tahun_pt');
        $jenis_pekerjaan = $request->input('jenis_krj');
        
        $nama_kantor = $request->input('nama_kantor');
        $alamat_kantor = $request->input('alamat_kantor');

        $pengalaman_kerja1 = $request->input('nama_kantorx1');
        $thn_pengalaman_kerja1 = $request->input('tahun_kerjax1');
        $pengalaman_kerja2 = $request->input('nama_kantorx2');
        $thn_pengalaman_kerja2 = $request->input('tahun_kerjax2');
        $pengalaman_kerja3 = $request->input('nama_kantorx3');
        $thn_pengalaman_kerja3 = $request->input('tahun_kerjax3');
        $pengalaman_kerja4 = $request->input('nama_kantorx4');
        $thn_pengalaman_kerja4 = $request->input('tahun_kerjax4');
        $status = 0;


        $member = Member::where('user_id', '=', Auth::user()->id)->first();
       // return response()->json($member);
       if ($member == null) {
                DB::insert('insert into tbl_member ( NIK, user_id, user_register_id, complete_name, gol_darah, jenis_kelamin, tempat_lahir, tanggal_lahir, nama_ayah, nama_ibu, nama_pasangan, status_perkawinan, alamat, provinsi, kabupatenkota, kecamatan, kelurahan, kode_pos, jenis_pekerjaan, nama_kantor, alamat_kantor, telp, pendidikan_sd, thn_pendidikan_sd, pendidikan_smp, thn_pendidikan_smp, pendidikan_sma, thn_pendidikan_sma, pendidikan_sarjana, thn_pendidikan_sarjana, pengalaman_kerja1, thn_pengalaman_kerja1, pengalaman_kerja2, thn_pengalaman_kerja2, pengalaman_kerja3, thn_pengalaman_kerja3, pengalaman_kerja4, thn_pengalaman_kerja4, status) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[$NIK,$user_id,$user_register_id,$complete_name,$gol_darah,$jenis_kelamin,$tempat_lahir,$tanggal_lahir,$nama_ayah,$nama_ibu,$nama_pasangan,$status_perkawinan,$alamat,$provinsi,$kabupatenkota,$kecamatan,$kelurahan,$kode_pos,$jenis_pekerjaan,$nama_kantor,$alamat_kantor,$telp,$pendidikan_sd,$thn_pendidikan_sd,$pendidikan_smp,$thn_pendidikan_smp,$pendidikan_sma,$thn_pendidikan_sma,$pendidikan_sarjana,$thn_pendidikan_sarjana,$pengalaman_kerja1,$thn_pengalaman_kerja1,$pengalaman_kerja2,$thn_pengalaman_kerja2,$pengalaman_kerja3,$thn_pengalaman_kerja3,$pengalaman_kerja4,$thn_pengalaman_kerja4,$status]);
        } else
        {
                

                DB::update('update tbl_member set NIK=?, complete_name=?, gol_darah=?, jenis_kelamin=?, tempat_lahir=?, tanggal_lahir=?, nama_ayah=?, nama_ibu=?, nama_pasangan=?, status_perkawinan=?, alamat=?, provinsi=?, kabupatenkota=?, kecamatan=?, kelurahan=?, kode_pos=?, jenis_pekerjaan=?, nama_kantor=?, alamat_kantor=?, telp=?,  pendidikan_sd=?, thn_pendidikan_sd=?, pendidikan_smp=?, thn_pendidikan_smp=?, pendidikan_sma=?, thn_pendidikan_sma=?, pendidikan_sarjana=?, thn_pendidikan_sarjana=?, pengalaman_kerja1=?, thn_pengalaman_kerja1=?, pengalaman_kerja2=?, thn_pengalaman_kerja2=?, pengalaman_kerja3=?, thn_pengalaman_kerja3=?, pengalaman_kerja4=?, thn_pengalaman_kerja4=?, status=? where user_id = ?',[$NIK, $complete_name,$gol_darah,$jenis_kelamin,$tempat_lahir,$tanggal_lahir,$nama_ayah,$nama_ibu,$nama_pasangan,$status_perkawinan,$alamat,$provinsi,$kabupatenkota,$kecamatan,$kelurahan,$kode_pos,$jenis_pekerjaan,$nama_kantor,$alamat_kantor,$telp,$pendidikan_sd,$thn_pendidikan_sd,$pendidikan_smp,$thn_pendidikan_smp,$pendidikan_sma,$thn_pendidikan_sma,$pendidikan_sarjana,$thn_pendidikan_sarjana,$pengalaman_kerja1,$thn_pengalaman_kerja1,$pengalaman_kerja2,$thn_pengalaman_kerja2,$pengalaman_kerja3,$thn_pengalaman_kerja3,$pengalaman_kerja4,$thn_pengalaman_kerja4,$status,$user_id]);
        }

    
        /*
        return response()->json([$NIK,$complete_name,$gol_darah,$jenis_kelamin,$tempat_lahir,$tanggal_lahir,$status_perkawinan,$nama_pasangan,$nama_ayah,$nama_ibu,$alamat,$provinsi,$kabupatenkota,$kecamatan,$kelurahan,$kode_pos,$telp,$email,$pendidikan_sd,$thn_pendidikan_sd,$pendidikan_smp,$thn_pendidikan_smp,$pendidikan_sma,$thn_pendidikan_sma,$pendidikan_sarjana,$thn_pendidikan_sarjana,$jenis_pekerjaan,$nama_kantor,$alamat_kantor,$pengalaman_kerja1,$thn_pengalaman_kerja1,$pengalaman_kerja2,$thn_pengalaman_kerja2,$pengalaman_kerja3,$thn_pengalaman_kerja3,$pengalaman_kerja4,$thn_pengalaman_kerja4]);
        */

          $id = Auth::user()->id;
        $pointer=DB::select("CALL GetMemberCheck(?)",[$id]);
         $member=DB::select("CALL GetMemberProfile(?)",[$id]);
      return view('member.profil')->with("status","success")->with('member',$member[0])->with('pointer',$pointer[0]->counter);
    }
    public function dokumenlist()
    {
        $id = Auth::user()->id;
        $doc=DB::select("CALL MemberDocumentList(?)",[$id]);

        //return response()->json($doc);
        return view('member.dokumen')->with('docs',$doc);
    }
    public function dokumenupload()
    {
        return view('member.upload_dokumen');
    }

    public function dokumenuploadproc(Request $request)
    {
        //

         $rules = [
            'jns_dok' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg',
         
        ];
            
             $messages = [
                    'jns_dok.required' => 'Jenis dokumen belum dipilih',
                    'foto.required' => 'Foto belum di pilih',
                ];

         $request->validate($rules,$messages);
     
    
            $user_id=Auth::user()->id;
            $kategori   = $request->jns_dok;
            $foto = date('YmdHis')."-".$user_id.".".$request->foto->getClientOriginalExtension();
            $status = "0";

            $pointer=DB::select("CALL InsertMemberDocument(?,?,?,?,@flag)",[$user_id,$kategori,$foto,$status]);
            $request->foto->move(public_path('member/dokumen'), $foto);
            $pointer2=DB::select("SELECT @flag");



            if($pointer2=="success"){
                return redirect()->route('/member/dokumenlist')->with('status','Dokumen berhasil di upload');
            }else{
                return redirect()->route('/member/dokumenlist')->with('status','Dokumen gagal di upload');
            }
            
    }
    public function dokumendelete($ids)
    {      
        
        $idx = Auth::user()->id;
        $mem=DB::select("CALL MemberDocumentById(?,?)",[$idx,$ids]);

        $file = public_path('member/dokumen/').$mem[0]->document_name;
        unlink($file);

        $delmember=DB::select("CALL MemberDocumentDelete(?,@flag)",[$ids]);

        $delmemberflag = DB::select("SELECT @flag");
         
        if($delmemberflag="success"){
            return redirect()->route('/member/dokumenlist')->with('status','Dokumen berhasil di hapus');
        }else{
            return redirect()->route('/member/dokumenlist')->with('status','Dokumen gagal di hapus');
        }
        

        //return response()->json([$id]);

        
    }
    public function pendaftaranunit()
    {
        return view('member.pendaftaranunit');
    }
    public function pendaftaranunitproc(Request $request)
    {
        
         $rules = [
            'kaliber_unit' => 'required',
            'tipe_unit' => 'required',
            'merek_unit' => 'required',
            'mekanisme_unit' => 'required',
            'foto_unit' => 'required|image|mimes:jpeg,png,jpg',
        ];
            
             $messages = [
                    'kaliber_unit.required' => 'kaliber unit belum dipilih',
                    'tipe_unit.required' => 'tipe unit belum dipilih',
                    'merek_unit.required' => 'merek unit belum di isi',
                    'mekanisme_unit.required' => 'mekanisme unit belum dipilih',
                    'foto_unit.required' => 'foto unit belum dipilih',
                ];

         $request->validate($rules,$messages);
        
    
            $user_id=Auth::user()->id;
            $kaliber_unit   = $request->kaliber_unit;
            $tipe_unit = $request->tipe_unit;
            $merek_unit = $request->merek_unit;
            $mekanisme_unit = $request->mekanisme_unit;
            $foto_unit = date('YmdHis')."-".$user_id.".".$request->foto_unit->getClientOriginalExtension();
            $status_unit = "0";

            //return response()->json([$user_id,$kaliber_unit,$tipe_unit,$merek_unit,$mekanisme_unit,$foto_unit,$status_unit]);
            
            $pointer=DB::select("CALL InsertUnit(?,?,?,?,?,?,?,@flag)",[$user_id,$kaliber_unit,$tipe_unit,$merek_unit,$mekanisme_unit,$foto_unit,$status_unit]);
            $pointer2=DB::select("SELECT @flag");
            $request->foto_unit->move(public_path('member/unit'), $foto_unit);
            


            
            if($pointer2="success"){
                return back()->with('status','Unit berhasil di daftarkan');
            }else{
                return back()->with('status','Unit gagal di daftarkan');
            }
      
    }

    public function daftarunit()
    {
         $id = Auth::user()->id;
        $units=DB::select("CALL UnitList(?)",[$id]);

        return view('member.daftarunit')->with('units',$units);
    }
    public function deleteunit($ids)
    {
               $idx = Auth::user()->id;
        $mem=DB::select("CALL UnitById(?,?)",[$idx,$ids]);

        $file = public_path('member/unit/').$mem[0]->foto;
        unlink($file);

        $delunit=DB::select("CALL UnitDelete(?,@flag)",[$ids]);

        $delunitflag = DB::select("SELECT @flag");
         
        if($delunitflag="success"){
            return back()->with('status','Unit berhasil di hapus');
        }else{
            return back()->with('status','Unit gagal di hapus');
        }
    }
    public function daftarklub()
    {
        return view('member.pendaftaranklub');
    }
    public function loadDataMember(Request $request)
    {

            $cari = $request->q;
            $data = DB::table('users')->select('id', 'username')
            ->where('username', 'LIKE', '%'.$cari.'%')
            ->where('username', '!=', 'root')
            ->take(5)
            ->get();
            return response()->json($data);
        
    }

    public function daftarklubproc(Request $request)
    {
        //
        
         $rules = [
            'nama_klub' => 'required',
            'alamat_klub' => 'required',
            'ketua' => 'required',
            'sekretaris' => 'required',
            'bendahara' => 'required'
        ];
            
             $messages = [
                    'nama_klub.required' => 'Nama Klub belum di isi',
                    'alamat_klub.required' => 'Alamat Klub belum di isi',
                    'ketua.required' => 'Username Ketua belum diisi',
                    'sekretaris.required' => 'Username Sekretaris belum diisi',
                    'bendahara.required' => 'Username Bendahara belum diisi', 
                ];

         $request->validate($rules,$messages);
        
        
            $nama_klub = $request->nama_klub;
            $alamat_klub = $request->alamat_klub;
            $ketua = $request->ketua;
            $sekretaris = $request->sekretaris;
            $bendahara = $request->bendahara;
            $creator_id = Auth::user()->id; 


            $pointer=DB::select("CALL InsertClub(?,?,?,?,?,?,@flag)",[$nama_klub,$alamat_klub,$ketua,$sekretaris,$bendahara,$creator_id]);

            $pointer2=DB::select("SELECT @flag");



            if($pointer2="success"){
                return redirect()->route('/member/daftarklub')->with('status','Club telah di registrasi, silahkan menunggu verifikasi');
            }else{
                return redirect()->route('/member/daftarklub')->with('status','Club gagal di registrasi');
            }
            
            //return response()->json([$nama_klub,$alamat_klub,$ketua,$sekretaris,$bendahara]);
    }
    public function manageklub()
    {
         $id = Auth::user()->id;
        $klub=DB::select("CALL ClubList(?)",[$id]);

        //return response()->json($klub);

        return view('member.manageklub')->with('klub',$klub);
    }
    public function manageklubdetail($id)
    {

        $klub=DB::select("CALL ClubDetail(?)",[$id]);
        $memberklub=DB::select("CALL MemberClubList(?)",[$id]);
        //return response()->json($klub);

        return view('member.manageklubdetail')->with('klub',$klub)->with('memberklub',$memberklub);
    }
    public function manageklubupdate($id)
    {
         $klub=DB::select("CALL ClubDetail(?)",[$id]);

        //return response()->json($pointer[0]);

        return view('member.manageklubupdate')->with('klub',$klub[0]);
    }
    public function manageklubupdateproc(Request $request)
    {
         $messages = [
                    'nama_klub.required' => 'Nama Klub belum terisi',
                    'alamat.required' => 'Alamat Klub belum di isi',
                    'ketua.required' => 'Ketua belum diisi',
                    'sekretaris.required' => 'Sekretaris belum diisi',
                    'bendahara.required' => 'Bendahara belum diisi',
                ];

        $rules = [
             'nama_klub' => 'required',
             'alamat' => 'required',
             'ketua' => 'required',
             'sekretaris' => 'required',
             'bendahara' => 'required',
        ];

        //validation
       $request->validate($rules,$messages);

            //to tbl_member
        $id =  $request->input('id');
        $nama_klub = $request->input('nama_klub');
        $alamat = $request->input('alamat');
        $ketua = $request->input('ketua');
        $sekretaris = $request->input('sekretaris');
        $bendahara = $request->input('bendahara');
          
        //return response()->json([$id,$nama_klub,$alamat,$ketua,$sekretaris,$bendahara]);
              
        $pointer=DB::select("CALL UpdateClub(?,?,?,?,?,?,@flag)",[$id,$nama_klub,$alamat,$ketua,$sekretaris,$bendahara]);

            $pointer2=DB::select("SELECT @flag");

            if($pointer2="success"){
                return back()->with('status','Club telah di update');
            }else{
                return back()->with('status','Club gagal di update');
            }
   
    }
    public function manageklubdelete($ids)
    {      
        
        $mem=DB::select("CALL ClubDetail(?)",[$ids]);


        $delclub=DB::select("CALL DeleteClub(?,@flag)",[$ids]);

        $delclubflag = DB::select("SELECT @flag");
         
        if($delclubflag="success"){
            return redirect('/member/manageklub')->with('status','Club berhasil di hapus');
        }else{
            return redirect('/member/manageklub')->with('status','Club gagal di hapus');
        }

        //return response()->json([$id]);

        
    }
    public function daftarmemberklub($id)
    {
        return view('member.addmember')->with('club_id',$id);
    }
     public function daftarmemberklubproc(Request $request)
    {
        //
        
         $rules = [
            'user_id' => 'required',
        ];
            
             $messages = [
                    'user_id.required' => 'username anggota belum di isi',
                ];

         $request->validate($rules,$messages);
        
        
            $club_id = $request->club_id;
            $user_id = $request->user_id;

        
            $pointer=DB::select("CALL InsertMemberClub(?,?,@flag)",[$club_id,$user_id]);

            $pointer2=DB::select("SELECT @flag");



            if($pointer2="success"){
                return back()->with('status','Member telah di daftarkan');
            }else{
                return back()->route('/member/daftarklub')->with('status','Member gagal di daftarkan');
            }
        
            
            //return response()->json([$club_id,$user_id]);
    }

    public function memberklubdelete($ids)
    {      


        $delmember=DB::select("CALL DeleteClubMember(?,@flag)",[$ids]);
        $delmemberflag = DB::select("SELECT @flag");
         
        if($delclubflag="success"){
            return back()->with('status','Member berhasil di hapus');
        }else{
            return back()->with('status','Member gagal di hapus');
        }

        //return response()->json([$id]);

        
    }
    public function pemusnahanunit()
    {
        return view('member.pemusnahanunit');
    }

}
