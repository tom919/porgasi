<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Member;
use DB;
use Auth;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('admin.home');
    }
    public function m_member()
    {
        $page = Input::get('page', 1);
        $paginate = 5;
        $data=DB::select("CALL AdmMemberList()");

        $offSet = ($page * $paginate) - $paginate;  
        $itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);  

        $users = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page); 
        $users->setPath('managementmember');
        //return response()->json($klub);

        return view('admin.m_member')->with('users',$users);
    }
    public function m_member_detail($id)
    {
        $page = Input::get('page', 1);
        $paginate = 5;
        $data=DB::select("CALL AdmMemberDetail(?)",[$id]);
         $doc=DB::select("CALL MemberDocumentList(?)",[$id]);
        //return response()->json($member);

        return view('admin.m_member_detail')->with('data',$data)->with('doc',$doc);
    }
    public function m_member_update_reg($id)
    {
        $member=DB::select("CALL GetMemberProfile(?)",[$id]);

            if($member == NULL)
        {
            return view('admin.m_member_update_reg');
        }
            else
        {
            return view('admin.m_member_update_reg')->with('user',$member[0]);
        }
        //return response()->json($member[0]);

        
        
    }
    public function m_member_update_reg_proc(Request $request)
    {


         $messages = [
                    'user_register_id.required' => 'Kode Registrasi belum terisi',
                ];

        $rules = [
             'user_register_id' => 'required',
        ];

        //validation
       $request->validate($rules,$messages);

            //to tbl_member
        $id =  $request->input('id');
        $user_register_id = $request->input('user_register_id');

        //return response()->json([$id,$user_register_id]);
          
            
        $pointer=DB::select("CALL AdmUpdateMemberRegCode(?,?,@flag)",[$id,$user_register_id]);

            $pointer2=DB::select("SELECT @flag");

            if($pointer2="success"){
                return back()->with('status','Kode Registrasi telah di update');
            }else{
                return back()->with('status','Kode Registrasi gagal di update');
            }
        
    }
    public function m_member_update_status($id)
    {

        $member=DB::select("CALL GetMemberProfile(?)",[$id]);

              if($member == NULL)
        {
            return view('admin.m_member_update_status');
        }
            else
        {
             return view('admin.m_member_update_status')->with('user',$member[0]);
        }
        //return response()->json($pointer[0]);

       
   
        
    }
      public function m_member_update_status_proc(Request $request)
    {


        
         $messages = [
                    'status.required' => 'Status User belum dipilih',
                ];

        $rules = [
             'status' => 'required',
        ];

        //validation
       $request->validate($rules,$messages);

            //to tbl_member
        $id =  $request->input('id');
        $user_status = $request->input('status');

        //return response()->json([$id,$user_register_id]);
          
            
        $pointer=DB::select("CALL AdmUpdateMemberStatus(?,?,@flag)",[$id,$user_status]);

            $pointer2=DB::select("SELECT @flag");

            if($pointer2="success"){
                return back()->with('status','User Status telah di update');
            }else{
                return back()->with('status','User Status gagal di update');
            }
        
        
    }
    public function m_member_update_status_dokumen($uid,$id)
    {

        $dok=DB::select("CALL MemberDocumentById(?,?)",[$uid,$id]);


        //return response()->json($pointer[0]);

        return view('admin.m_member_update_dokumen_status')->with('dok',$dok[0]);
   
        
    }
    public function m_member_update_status_dokumen_proc(Request $request)
    {


          $messages = [
                    'status.required' => 'Status Dokumen belum dipilih',
                ];

        $rules = [
             'status' => 'required',
        ];

        //validation
       $request->validate($rules,$messages);

            //to tbl_member
        $id =  $request->input('id');
        $dok_status = $request->input('status');

        //return response()->json([$id,$user_register_id]);
          
            
        $pointer=DB::select("CALL AdmUpdateMemberDocStatus(?,?,@flag)",[$id,$dok_status]);

            $pointer2=DB::select("SELECT @flag");

            if($pointer2="success"){
                return back()->with('status','Dokumen Status telah di update');
            }else{
                return back()->with('status','Dokumen Status gagal di update');
            }
        
   
        
    }
    public function m_member_delete_member($ids)
    {


        $mem=DB::select("CALL ClubDetail(?)",[$ids]);


        $delclub=DB::select("CALL AdmDeleteMember(?,@flag)",[$ids]);

        $delclubflag = DB::select("SELECT @flag");
         
        if($delclubflag="success"){
            return redirect('/admin/managementmember')->with('status','Member berhasil di hapus');
        }else{
            return redirect('/admin/managementmember')->with('status','Member gagal di hapus');
        }
   
        
    }
    public function m_member_search(Request $request)
    {

         $page = Input::get('page', 1);
        $paginate = 5;

        $query = filter_var($request->input('val'), FILTER_SANITIZE_STRING);
        $field = $request->input('field');

        switch ($field) {
        case "1":
            $fieldx='tbl_member.complete_name';
            break;
        case "2":
            $fieldx='tbl_member.user_register_id';
            break;
        case "3":
            $fieldx='tbl_member.kabupatenkota';
            break;
         case "4":
            $fieldx='tbl_member.telp';
            break;
         case "5":
            $fieldx='tbl_member.status';
            break;
        default:
            $fieldx='tbl_member.complete_name';
        }

        if ($field = "5" ) {
            
            switch ($query) {
            case "menunggu pemeriksaan":
                $query='0';
                break;
            case "aktif":
                $query='1';
                break;
            case "suspend":
                $query='2';
                break;
            case "terblokir":
                $query='3';
                break;
             case "data tidak lengkap":
                $query='4';
                break;
             case "data terindikasi palsu":
                $query='5';
                break;
            }
        }

        $mr=DB::select(DB::raw("SELECT users.id AS id,
                    users.username AS username,
                    CekRole(users.role_id) AS role,
                    tbl_member.NIK AS NIK,
                    tbl_member.user_id AS user_id,
                    tbl_member.user_register_id AS user_register_id,
                    tbl_member.complete_name AS complete_name,
                    CekGender(tbl_member.jenis_kelamin) AS jenis_kelamin,
                    tbl_member.gol_darah AS gol_darah,
                    CekStatNikah(tbl_member.status_perkawinan) AS status_perkawinan,
                    tbl_member.jenis_pekerjaan AS jenis_pekerjaan_id,
                    CekKerja(tbl_member.jenis_pekerjaan) AS jenis_pekerjaan,
                    tbl_member.telp AS telp,
                    users.email AS email,
                    CekMemberStatus(tbl_member.status) AS status 
                    FROM users
                        JOIN tbl_member
                        ON users.id = tbl_member.user_id

                     WHERE $fieldx LIKE '%$query%'"));


        //return response()->json($mr);
        $offSet = ($page * $paginate) - $paginate;  
        $itemsForCurrentPage = array_slice($mr, $offSet, $paginate, true);  

        $mr = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($mr), $paginate, $page); 
        $mr->setPath('managementmembersearch');

        return view('admin.m_member_search')->with('users',$mr);
   
        
    }
    public function m_klub()
    {
        $page = Input::get('page', 1);
        $paginate = 5;
        $data=DB::select("CALL AdmClubList()");

        $offSet = ($page * $paginate) - $paginate;  
        $itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);  

        $clubs = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page); 
        $clubs->setPath('managementclub');
        //return response()->json($klub);

        return view('admin.m_klub')->with('clubs',$clubs);
    }
     public function m_klub_search(Request $request)
    {

         $page = Input::get('page', 1);
        $paginate = 5;

        $query = filter_var($request->input('valxs'), FILTER_SANITIZE_STRING);
        $field = $request->input('fieldxs');

        switch ($field) {
        case "1":
            $fieldx='tbl_club.club_id';
            break;
        case "2":
            $fieldx='tbl_club.nama_klub';
            break;
        case "3":
            $fieldx='tbl_club.alamat_klub';
            break;
         case "4":
            $fieldx='tbl_club.status';
            break;
        }

        if ($field == "4" ) {
            
            switch ($query) {
            case "menunggu pemeriksaan":
                $query='0';
                break;
            case "di setujui":
                $query='1';
                break;
            case "ditolak":
                $query='2';
                break;
            case "mohon di perbaiki":
                $query='3';
                break;
            default:
                $query='0';
            }
        }

        $mr=DB::select(DB::raw("SELECT 

                    club_id,
                    nama_klub,
                    status AS status_id,                    
                    CekKlubStatus(status) AS status 

                    FROM tbl_club WHERE $fieldx LIKE '%$query%'"));


        //return response()->json([$query,$fieldx]);
     
        $offSet = ($page * $paginate) - $paginate;  
        $itemsForCurrentPage = array_slice($mr, $offSet, $paginate, true);  

        $mr = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($mr), $paginate, $page); 
        $mr->setPath('managementklubsearch');

        return view('admin.m_klub_search')->with('clubs',$mr);
        
   
        
    }
    public function m_klub_detail($id)
    {

        $klub=DB::select("CALL ClubDetail(?)",[$id]);
        $memberklub=DB::select("CALL MemberClubList(?)",[$id]);
        //return response()->json($klub);

        return view('admin.m_klub_detail')->with('klub',$klub)->with('memberklub',$memberklub);
    }
    public function m_klub_update($id)
    {
         $klub=DB::select("CALL ClubDetail(?)",[$id]);

        //return response()->json($pointer[0]);

        return view('admin.m_klub_update')->with('klub',$klub[0]);
    }
      public function m_klub_update_proc(Request $request)
    {
         

            //to tbl_club
        $id =  $request->input('id');
        $status = $request->input('status');
          
        //return response()->json([$id,$status]);
   
        $pointer=DB::select("CALL AdminUpdateClubStatus(?,?,@flag)",[$id,$status]);

            $pointer2=DB::select("SELECT @flag");

            if($pointer2="success"){
                return back()->with('status','Club Status telah di update');
            }else{
                return back()->with('status','Club Status di update');
            }
   
    }
     public function m_klub_delete($ids)
    {      
        

        $delclub=DB::select("CALL DeleteClub(?,@flag)",[$ids]);

        $delclubflag = DB::select("SELECT @flag");
         
        if($delclubflag="success"){
            return redirect('admin/managementclub')->with('status','Club berhasil di hapus');
        }else{
            return redirect('admin/managementclub')->with('status','Club gagal di hapus');
        }

        //return response()->json([$id]);

        
    }
    public function m_daftar_unit()
    {
        $page = Input::get('page', 1);
        $paginate = 5;

        $data=DB::select("CALL AdmUnitList()");

        

        $offSet = ($page * $paginate) - $paginate;  
        $itemsForCurrentPage = array_slice($data, $offSet, $paginate, true);  

        $units = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page); 
        $units->setPath('./daftarunit');

        return view('admin.m_member_unit')->with('units',$units);
    }
      public function m_unit_search(Request $request)
    {

         $page = Input::get('page', 1);
        $paginate = 5;

        $query = filter_var($request->input('valxs'), FILTER_SANITIZE_STRING);
        $field = $request->input('fieldxs');

        switch ($field) {
        case "1":
            $fieldx='tbl_unit.unit_id';
            break;
        case "2":
            $fieldx='tbl_unit.kaliber';
            break;
        case "3":
            $fieldx='users.username';
            break;
         case "4":
            $fieldx='tbl_unit.status';
            break;
        case "5":
            $fieldx='tbl_unit.unit_type';
            break;
        }

        if ($field == "4" ) {
            
            switch ($query) {
            case "Menunggu Pemeriksaan":
                $query='0';
                break;
            case "Aktif":
                $query='1';
                break;
            case "Proses Pemusnahan":
                $query='2';
                break;
            case "Proses Transfer Pemilik":
                $query='3';
                break;
            case "Data Salah":
                $query='4';
                break;
            default:
                $query='0';
            }
        }

        $mr=DB::select(DB::raw("SELECT 

                    tbl_unit.unit_id AS id,
                    tbl_unit.kaliber AS kaliber,
                    tbl_unit.unit_type AS unit_type,
                    tbl_unit.merek AS merek,
                    tbl_unit.foto AS foto,
                    tbl_unit.status AS status_id,                    
                    CekUnitStatus(tbl_unit.status) AS status,
                    users.id AS pemilik_id,
                    users.username AS pemilik 

                    FROM tbl_unit

                    JOIN users ON tbl_unit.user_id = users.id

                     WHERE $fieldx LIKE '%$query%'"));


        //return response()->json([$mr]);
        
        $offSet = ($page * $paginate) - $paginate;  
        $itemsForCurrentPage = array_slice($mr, $offSet, $paginate, true);  

        $mr = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($mr), $paginate, $page); 
        $mr->setPath('managementunitsearch');

        return view('admin.m_member_unit_search')->with('units',$mr);
        
   
        
    }
    public function m_unit_detail($id)
    {

        $units=DB::select("CALL AdmUnitById(?)",[$id]);
        //return response()->json($klub);

        return view('admin.m_member_unit_detail')->with('units',$units);
    }
    public function m_unit_update_reg($id)
    {
        $units=DB::select("CALL AdmUnitById(?)",[$id]);

        //return response()->json($pointer[0]);

        return view('admin.m_unit_update_reg')->with('units',$units[0]);
    }
    public function m_unit_update_reg_proc(Request $request)
    {

            //to tbl_club
        $id =  $request->input('id');
        $reg_id = $request->input('reg_id');
          
        //return response()->json([$id,$status]);
   
        $pointer=DB::select("CALL AdmUpdateUnitReg(?,?,@flag)",[$id,$reg_id]);

            $pointer2=DB::select("SELECT @flag");

            if($pointer2="success"){
                return back()->with('status','Nomer Registrasi Unit telah di update');
            }else{
                return back()->with('status','Nomer Registrasi Unit  gagal di update');
            }
    }
    public function m_unit_update_status($id)
    {
        $units=DB::select("CALL AdmUnitById(?)",[$id]);

        //return response()->json($pointer[0]);

        return view('admin.m_unit_update_stat')->with('units',$units[0]);
    }
    public function m_unit_update_status_proc(Request $request)
    {

            //to tbl_unit
        $id =  $request->input('id');
        $status = $request->input('status');
          
        //return response()->json([$id,$status]);
   
        $pointer=DB::select("CALL AdmUpdateUnitStat(?,?,@flag)",[$id,$status]);

            $pointer2=DB::select("SELECT @flag");

            if($pointer2="success"){
                return back()->with('status','Status Unit telah di update');
            }else{
                return back()->with('status','Status Unit  gagal di update');
            }
    }
    public function m_unit_delete($ids)
    {      
        
        $mem=DB::select("CALL AdmUnitById(?)",[$ids]);
        $delunit=DB::select("CALL AdmDeleteUnit(?,@flag)",[$ids]);

        $delunitflag = DB::select("SELECT @flag");
         
        $file = public_path('member/unit/').$mem[0]->foto;
        unlink($file);

        if($delunitflag="success"){
            return back()->with('status','Unit berhasil di hapus');
        }else{
            return back()->with('status','Unit gagal di hapus');
        }

        //return response()->json([$id]);

        
    }
   
    public function m_polisi()
    {
        return view('admin.m_polisi');
    }

}
