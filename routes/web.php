<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/*Route::get('/home', 'HomeController@index')->name('home');*/
Route::get('/home', function(){
    switch(Auth::user()->role_id){
        case '3':
            return redirect('/home/member');
        case '2':
            return redirect('/homebase');
        case '1':
            return redirect('/home/admin');
        case '0':
            return redirect('/homebase');
        default : 
        	return redirect('/homebase');
    }
});
Route::get('/homebase', 'HomeController@index')->name('home');
Route::get('/setting', 'AccountController@setting');

//---------------Route Member------------------

Route::get('/home/member', 'MemberController@home');
Route::get('/member/profil', 'MemberController@profil');
Route::get('/member/insertprofil', 'MemberController@insertProfil');
Route::get('/member/updateprofil', 'MemberController@updateProfil');
Route::post('/member/updateprofilproc', 'MemberController@updateProfilProcess');

Route::get('/member/dokumenlist', 'MemberController@dokumenlist')->name('/member/dokumenlist');
Route::get('/member/dokumenupload', 'MemberController@dokumenupload');
Route::post('/member/dokumenuploadproc', 'MemberController@dokumenuploadproc');
Route::get('/member/dokumendelete/{id}', 'MemberController@dokumendelete');



Route::get('/member/pendaftaranunit', 'MemberController@pendaftaranunit');
Route::post('/member/pendaftaranunitproc', 'MemberController@pendaftaranunitproc');
Route::get('/member/daftarunit', 'MemberController@daftarunit');
Route::get('/member/unitdeleteproc/{id}', 'MemberController@deleteunit');
Route::get('/member/pemusnahanunit', 'MemberController@pemusnahanunit');

Route::get('/member/daftarklub', 'MemberController@daftarklub')->name('/member/daftarklub');
Route::post('/member/daftarklubproc', 'MemberController@daftarklubproc');
Route::get('/member/manageklub', 'MemberController@manageklub');
Route::get('/member/manageklubdetail/{id}', 'MemberController@manageklubdetail');
Route::get('/member/manageklubdelete/{id}', 'MemberController@manageklubdelete');
Route::get('/manageklubupdate/{id}', 'MemberController@manageklubupdate');
Route::post('/manageklubupdateproc', 'MemberController@manageklubupdateproc');

Route::get('/addmember/{id}', 'MemberController@daftarmemberklub');
Route::post('/addmemberproc', 'MemberController@daftarmemberklubproc');
Route::get('/memberklubdelete/{id}', 'MemberController@memberklubdelete');
Route::get('/carimember', 'MemberController@loadDataMember');
//---------------End Of Route Member------------------


//---------------Route Admin-------------------
Route::group(['middleware' => ['auth', 'admin']], function() {
Route::get('/home/admin', 'AdminController@home');

Route::get('/admin/managementmember','AdminController@m_member');
Route::get('/admin/managementmemberdetail/{id}','AdminController@m_member_detail');
Route::get('/admin/managementmemberupdatereg/{id}','AdminController@m_member_update_reg');
Route::post('/managementmemberupdateregproc', 'AdminController@m_member_update_reg_proc');
Route::get('/admin/managementmemberupdatestatus/{id}','AdminController@m_member_update_status');
Route::post('/managementmemberupdatestatusproc', 'AdminController@m_member_update_status_proc');
Route::get('/admin/managementmemberupdatedocstatus/{uid}/{id}','AdminController@m_member_update_status_dokumen');
Route::post('/managementmemberupdatedocstatusproc', 'AdminController@m_member_update_status_dokumen_proc');
Route::post('/managementmembersearch', 'AdminController@m_member_search');
Route::get('/managementmemberdelete/{id}', 'AdminController@m_member_delete_member');

Route::get('/admin/managementclub','AdminController@m_klub');
Route::post('/adminclubsearch', 'AdminController@m_klub_search');

Route::get('/admin/manageklubdetail/{id}', 'AdminController@m_klub_detail');
Route::get('/adminclubupdate/{id}', 'AdminController@m_klub_update');
Route::post('/adminclubupdateproc', 'AdminController@m_klub_update_proc');
Route::get('/adminklubdelete/{id}', 'AdminController@m_klub_delete');

Route::get('/admin/daftarunit', 'AdminController@m_daftar_unit');
Route::get('/admin/unitdetail/{id}', 'AdminController@m_unit_detail');
Route::get('/admin/unitupdatereg/{id}','AdminController@m_unit_update_reg');
Route::post('/unitupdateregproc', 'AdminController@m_unit_update_reg_proc');
Route::get('/admin/unitupdatestatus/{id}','AdminController@m_unit_update_status');
Route::post('/unitupdatestatusproc', 'AdminController@m_unit_update_status_proc');
Route::post('/managementunitsearch', 'AdminController@m_unit_search');
Route::get('/adminunitdelete/{id}', 'AdminController@m_unit_delete');

Route::get('/admin/managementpolisi','AdminController@m_polisi');
});
//-------------- End Of Route Admin ------------