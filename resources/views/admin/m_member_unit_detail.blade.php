@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                     <h2>Unit Detail</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="table-responsive">        
                      <table class="table">
                        <tbody>
                          @foreach($units as $u)
                          <tr>
                            <td>ID Klub</td>
                            <td>{{$u->id}}</td>
                          </tr>
                          <tr>

                            <td>Nama Klub</td>
                            <td><img class="zoom" src="{{asset('member/unit')}}/{{$u->foto}}"></td>
                          </tr>
                          <tr>
                            <td>Pemilik</td>
                            <td>{{$u->pemilik}}</td>
                          </tr>
                          <tr>
                          <tr>
                            <td>Nomor Registrasi</td>
                            <td>{{$u->register_id}}</td>
                          </tr>
                          <tr>
                            <td>Kaliber</td>
                            <td>{{$u->kaliber}}</td>
                          </tr>
                          <tr>
                            <td>Tipe</td>
                            <td>{{$u->unit_type}}</td>
                          </tr>
                          <tr>
                            <td>Merek</td>
                            <td>{{$u->merek}}</td>
                          </tr>
                           <tr>
                            <td>Mekanisme</td>
                            <td>{{$u->mekanisme}}</td>
                          </tr>
                           <tr>
                            <td>Tanggal Daftar</td>
                            <td>{{$u->created_at}}</td>
                          </tr>
                          <tr><td></td>
                            <td>
                              <a href="{{URL::to('/')}}/admin/unitupdatereg/{{$u->id}}"><button class="btn btn-sm btn-default"><span class="glyphicon glyphicon-barcode"></span>&nbsp;Update Kode Registrasi</button></a>

                              <a href="{{URL::to('/')}}/admin/unitupdatestatus/{{$u->id}}"><button class="btn btn-sm btn-default"><span class="glyphicon glyphicon-ok-circle"></span>&nbsp;Update Status Unit</button></a>

                            <a href="{{URL::to('/')}}//adminunitdelete/{{$u->id}}"><button class="btn btn-danger btn-sm" onclick="return confirm('anda yakin hapus unit ini?')"><span class="glyphicon glyphicon-trash"></span>&nbsp;Hapus</button></a></td>
                          </tr>
                            @endforeach
                        </tbody>
                      </table>
                      </div>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
