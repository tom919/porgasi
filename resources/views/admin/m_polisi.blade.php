@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                     <h2>Daftar Polisi</h2>
                         <div class="pull-right">
                      <button class="btn btn-primary"><i class="fa fa-user-plus"></i>&nbsp;Tambah</button>
                    </div>  

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-inline">
                      <input type="text" name="searchval" class="form-control " placeholder="kata kunci...">
                                <select name="field" class="form-control">
                      <option selected>berdasarkan</option>
                      <option>Nama</option>
                      <option>No Hp</option>
                      <option>Kesatuan</option>
                      <option>Email</option>
                      <option>Status Keanggotaan</option>
                    </select>
                    <button type="submit" name="submit" class="btn" value="cari" style="margin-top: 0.5%">Cari &nbsp;<span class="glyphicon glyphicon-search"></span></button>
                    </form>

                    <br>
                     <div class="table-responsive">        
                      <table class="table">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>No Hp</th>
                            <th>Kesatuan</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>Ahamad </td>
                            <td>0819999999</td>
                            <td>Intelkam</td>
                            <td>ahmad@gmail.com</td>
                            <td class="bg-success"><b>Aktif</b></td>
                            <td><button class="btn btn-default btn-sm">Detail &nbsp; <i class="fa fa-eye"></i></button>
                            <a href="#"><button class="btn btn-primary btn-sm">Update &nbsp; <i class="fa fa-pencil-square-o"></i></button></a>
                            <button class="btn btn-danger btn-sm">Delete&nbsp;<i class="fa fa-trash"></i></button></td>
                          </tr>
                       
                        </tbody>
                      </table>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
