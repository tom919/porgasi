@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                     <h2>Pencarian Klub</h2>


                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  

                    <br>
                     <div class="table-responsive">        
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Id</th>
                            <th>Nama Klub</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($clubs as $c)
                          <tr>
                            <td>{{$c->club_id}}</td>
                            <td>{{$c->nama_klub}}</td>
                            <td class="bg-success"><b>{{$c->status}}</b></td>
                            <td>
                              <a href="{{URL::to('/')}}/admin/manageklubdetail/{{$c->club_id}}"><button class="btn btn-default btn-sm">Detail</button></a><a href="{{URL::to('/')}}/adminklubdelete/{{$c->club_id}}"><button class="btn btn-danger btn-sm" onclick="return confirm('anda yakin hapus klub ini?')">
                           Delete</button></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {{ $clubs->links() }}
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
