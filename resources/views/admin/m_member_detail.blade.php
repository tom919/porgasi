@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Detail Akun</h2>



                     @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                              @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <br>
                   
                    <br>
                   <table class="table">
    <tbody>
      @foreach($data as  $member)
      <tr>
        <td>Username</td>
        <td>{{$member->username}}</td>
      </tr>
      <tr>
        <td>Role</td>
        <td>{{$member->role}}</td>
      </tr>
      <tr>
        <td>NIK</td>
        <td>{{$member->NIK}}</td>
      </tr>
      <tr>
        <td>Register ID</td>
        <td>{{$member->user_register_id}}</td>
      </tr>
      <tr>
        <td>Nama Lengkap</td>
        <td>{{$member->complete_name}}</td>
      </tr>
      <tr>
        <td>Golongan Darah</td>
        <td>{{$member->gol_darah}}</td>
      </tr>
      <tr>
        <td>Jenis Kelamin</td>
        <td>{{$member->jenis_kelamin}}</td>
      </tr>
      <tr>
        <td>Tempat dan Tanggal Lahir</td>
        <td>{{$member->tempat_lahir}}, {{$member->tanggal_lahir}}</td>
      </tr>
      <tr>
        <td>Nama Ayah</td>
        <td>{{$member->nama_ayah}}</td>
      </tr>
      <tr>
        <td>Nama Ibu</td>
        <td>{{$member->nama_ibu}}</td>
      </tr>
      <tr>
        <td>Status Perkawinan</td>
        <td>{{$member->status_perkawinan}}</td>
      </tr>
      <tr>
        <td>Nama Pasangan</td>
        <td>{{$member->nama_pasangan}}</td>
      </tr>
      <tr>
        <td>Alamat</td>
        <td>{{$member->alamat}}</td>
      </tr>
      <tr>
        <td>Provinsi</td>
        <td>{{$member->provinsi}}</td>
      </tr>
      <tr>
        <td>Kabupaten Kota</td>
        <td>{{$member->kabupatenkota}}</td>
      </tr>
      <tr>
        <td>Kecamatan</td>
        <td>{{$member->kecamatan}}</td>
      </tr>
      <tr>
        <td>Kelurahan</td>
        <td>{{$member->kelurahan}}</td>
      </tr>
      <tr>
        <td>Kode Pos</td>
        <td>{{$member->kode_pos}}</td>
      </tr>
      <tr>
        <td>Telp</td>
        <td>{{$member->telp}}</td>
      </tr>
      <tr>
        <td>Pendidikan SD</td>
        <td>{{$member->pendidikan_sd}} - {{$member->thn_pendidikan_sd}}</td>
      </tr>
      <tr>
        <td>Pendidikan SMP</td>
        <td>{{$member->pendidikan_smp}} - {{$member->thn_pendidikan_smp}}</td>
      </tr>
      <tr>
        <td>Pendidikan SMA</td>
        <td>{{$member->pendidikan_sma}} - {{$member->thn_pendidikan_sma}}</td>
      </tr>
      <tr>
        <td>Pendidikan Perguruan Tinggi</td>
        <td>{{$member->pendidikan_sarjana}} - {{$member->thn_pendidikan_sarjana}}</td>
      </tr>
      <tr>
        <td>Pekerjaan</td>
        <td>{{$member->jenis_pekerjaan}}</td>
      </tr>
      <tr>
        <td>Nama Instansi / Perusahaan</td>
        <td>{{$member->nama_kantor}}</td>
      </tr>
      <tr>
        <td>Alamat Kantor</td>
        <td>{{$member->alamat_kantor}}</td>
      </tr>
      <tr>
        <td>Pengalaman Kerja 1</td>
        <td>{{$member->pengalaman_kerja1}} - {{$member->thn_pengalaman_kerja1}}</td>
      </tr>
            <tr>
        <td>Pengalaman Kerja 2</td>
        <td>{{$member->pengalaman_kerja2}} - {{$member->thn_pengalaman_kerja2}}</td>
      </tr>
      <tr>
        <td>Pengalaman Kerja 3</td>
        <td>{{$member->pengalaman_kerja3}} - {{$member->thn_pengalaman_kerja3}}</td>
      </tr>
      <tr>
        <td>Pengalaman Kerja 4</td>
        <td>{{$member->pengalaman_kerja4}} - {{$member->thn_pengalaman_kerja4}}</td>
      </tr>
      <tr>
        <td>Status</td>
        <td>{{$member->status}}</td>
      </tr>
      <tr>
        <td></td>
      <td><a href="{{URL::to('/')}}/admin/managementmemberupdatereg/{{$member->id}}"><button class="btn btn-default"><span class="glyphicon glyphicon-barcode"></span>&nbsp;Update Register ID</button></a>

      <a href="{{URL::to('/')}}/admin/managementmemberupdatestatus/{{$member->id}}"><button class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span>&nbsp;Update Status</button></a>

      <button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span>&nbsp;Hapus Akun</button></td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <br>
      <div class="table-responsive">
                     <h3>Daftar Dokumen</h3>          
                      <table class="table">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Jenis Dokumen</th>
                            <th>Preview</th>
                            <th>Tanggal Upload</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($doc as $docs)
                          <tr>
                            <td>{{$docs->id}}</td>
                            <td>{{$docs->category}}</td>
                            <td><img class="zoom" src="{{asset('member/dokumen')}}/{{$docs->document_name}}"></td>
                            <td>{{$docs->created_at}}</td>
                            <td><b>{{$docs->status}}</b></td>
                           <td> <a href="{{URL::to('/')}}/admin/managementmemberupdatedocstatus/{{$member->id}}/{{$docs->id}}"><button class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span>&nbsp;Update Status</button></a></td>
                          </tr>
                            @endforeach
                        </tbody>
                      </table>
                      </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
