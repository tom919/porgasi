@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Update Status Member</h2>



                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                              @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                     @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                      @if(!empty($user))
                    <form class="form" method="post" action="{{ url('/managementmemberupdatestatusproc')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$user->user_id}}">
                         <div class="form-group">
                            <label class="col-md-6">Nama Lengkap :</label>
                            <label class="col-md-6">{{$user->complete_name}}</label>
                        </div>
                          <div class="form-group">
                            <label class="col-md-6">NIK :</label>
                            <label class="col-md-6">{{$user->NIK}}</label>
                        </div>
                          <div class="form-group">
                            <label class="col-md-6" >Tempat dan Tanggal Lahir :</label>
                            <label class="col-md-6">{{$user->tempat_lahir}}, {{$user->tanggal_lahir}}</label>
                        </div>
                     
                        <div class="form-group">
                            <label class="col-md-6" >Status User</label>
                            <div class="col-md-6">
                            <select class="form-control" name="status">
                              <option selected value="{{$user->status_id}}">{{$user->status}}</option>
                              <option value="0">Menunggu Pemeriksaan</option>
                              <option value="1">Aktif</option>
                              <option value="2">Suspend</option>
                              <option value="3">Terblokir</option>
                              <option value="4">Data Tidak Lengkap</option>
                              <option value="5">Data Terindikasi Palsu</option>
                            </select>
                            </div>
                        </div>
 
 
                        <br>
                        <div class="col-md-6"></div>
                        <div class="form-group col-md-6" style="padding: 1%;">
                          <input type="submit" class="btn btn-success pull-right" value="update">
                        </div>
                    </form>
                      @else
                      <div class="alert alert-warning">
                        <label>Data member belum di isi</label>
                    </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
