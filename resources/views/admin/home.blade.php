@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Admin Home</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
              
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
         <!--
                    <br>
                      <h4>Rekap Perijinan</h4>
                    <br>
                     <div class="clearfix"></div>
                      <div class="table-responsive">        
                      <table class="table">
                        <thead>
                        <tr>
                          <th>Jenis</th>
                          <th class="warning">Belum diproses</th>
                          <th class="success">disetujui</th>
                          <th class="danger">ditolak</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Registrasi Anggota</td>
                          <td class="warning">50</td>
                          <td class="success">5</td>
                          <td class="danger">1</td>
                        </tr>
                          <tr>
                          <td>Registrasi Klub</td>
                          <td class="warning">5</td>
                          <td class="success">2</td>
                          <td class="danger">0</td>
                        </tr>
                        <tr>
                          <td>Registrasi Unit</td>
                          <td class="warning">200</td>
                          <td class="success">30</td>
                          <td class="danger">7</td>
                        </tr>
                      </tbody>
                      
                      </table>
                      </div>

                  -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
