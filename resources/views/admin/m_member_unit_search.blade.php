@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Daftar Unit</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      
                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
   

                         @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                     <div class="table-responsive">
                           
                      <table class="table">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Pemilik</th>
                            <th>Tipe</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($units as $unit)
                          <tr>
                            <td>{{$unit->id}}</td>
                            <td>{{$unit->pemilik}}</td>
                            <td>{{$unit->unit_type}}</td>
                            <td><b>{{$unit->status}}</b></td>
                            <td>
                              <div class="col-md-6">
                              <a href="{{URL::to('/')}}/admin/unitdetail/{{$unit->id}}"><button class="btn btn-default btn-sm"><span class="glyphicon glyphicon-eye-open pull-left"></span>&nbsp;Lihat Detail</button></a>
                              </div>
                              <div class="col-md-6">
                              <a href="{{URL::to('/')}}//adminunitdelete/{{$unit->id}}"><button class="btn btn-danger btn-sm" onclick="return confirm('anda yakin hapus unit ini?')"><span class="glyphicon glyphicon-trash pull-left"></span>&nbsp;Hapus</button></a>
                              </div>
                            </td>
                          </tr>
                            @endforeach
                        </tbody>
                      </table>
                      {{ $units->links() }}
                      </div>
                    <br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
