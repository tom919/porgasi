@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                     <h2>Daftar Member</h2>
                     <div class="pull-right">
                      
                    </div>  

                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                <form class="form-inline" method="post" action="{{ url('/managementmembersearch')}}">
                  {{ csrf_field() }}
                      <input type="text" name="val" class="form-control " placeholder="kata kunci...">
                                <select name="field" class="form-control">
                      <option selected>berdasarkan</option>
                      <option value="1">Nama</option>
                      <option value="2">No Reg</option>
                      <option value="3">Kota</option>
                      <option value="4">No Hp</option>
                      <option value="5">Status Keanggotaan</option>
                    </select>
                    <button type="submit" name="submit" class="btn" value="cari" style="margin-top: 0.5%">Cari &nbsp;<span class="glyphicon glyphicon-search"></span></button>
                    </form>

                    <br>
                     <div class="table-responsive">        
                      <table class="table">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Nama Lengkap</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($users as $u)
                          <tr>
                            <td>{{$u->id}}</td>
                            <td>{{$u->username}}</td>
                            <td>{{$u->email}}</td>
                            <td>{{$u->complete_name}}</td>
                            <td>{{$u->status}}</td>
                             <td><a href="{{URL::to('/')}}/admin/managementmemberdetail/{{$u->id}}"><button class="btn btn-default btn-sm">Lihat Detail &nbsp; <i class="fa fa-eye"></i></button></a>
                            <a href="{{URL::to('/')}}/managementmemberdelete/{{$u->id}}"><button class="btn btn-danger btn-sm" onclick="return confirm('anda yakin hapus member ini?')">Hapus Akun e&nbsp;<i class="fa fa-trash"></i></button></a></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {{ $users->links() }}
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
