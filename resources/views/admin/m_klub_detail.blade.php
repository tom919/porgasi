@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                     <h2>Klub Detail</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="table-responsive">        
                      <table class="table">
                        <tbody>
                          @foreach($klub as $k)
                          <tr>
                            <td>ID Klub</td>
                            <td>{{$k->id}}</td>
                          </tr>
                          <tr>
                            <td>Nama Klub</td>
                            <td>{{$k->nama_klub}}</td>
                          </tr>
                          <tr>
                            <td>Ketua</td>
                            <td>{{$k->username_ketua}}</td>
                          </tr>
                          <tr>
                            <td>Sekretaris</td>
                            <td>{{$k->username_sekretaris}}</td>
                          </tr>
                          <tr>
                            <td>Bendahara</td>
                            <td>{{$k->username_bendahara}}</td>
                          </tr>
                          <tr>
                            <td>Status</td>
                            <td>{{$k->status}}</td>
                          </tr>
                          <tr><td></td>
                            <td><a href="{{URL::to('/')}}/adminclubupdate/{{$k->id}}"><button class="btn btn-sm btn-default"><span class="glyphicon glyphicon-barcode"></span>&nbsp;Update Status Klub</button></a><a href="{{URL::to('/')}}/adminklubdelete/{{$k->id}}"><button class="btn btn-danger btn-sm" onclick="return confirm('anda yakin hapus klub ini?')"><span class="glyphicon glyphicon-trash"></span>&nbsp;Hapus</button></a></td>
                          </tr>
                            @endforeach
                        </tbody>
                      </table>
                      </div>
                      <!--- member list -->
                      <div class="table-responsive">        
                      <h3>Daftar Anggota</h3>
                      <hr>
                      <br>
                      <table class="table">
                        <thead>
                          <th>ID</th>
                          <th>Name</th>
                          <th>Status</th>
                          <th></th>
                        </thead>
                        <tbody>
                           @foreach($memberklub as $kk)
                          <tr>
                            <td>{{$kk->id}}</td>
                            <td>{{$kk->username}}</td>
                            <td>{{$kk->status_member}}</td>
                            <td></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      </div>
                      <!--- end of member list -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
