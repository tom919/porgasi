@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Profil Akun Baru</h2>



                     @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                              @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="form" method="post" action="{{ url('/member/updateprofilproc')}}">
                        {{ csrf_field() }}
                          <div class="form-group">
                            <label >NIK</label>
                            <input type="text" class="form-control" name="NIK" placeholder="masukan NIK">
                        </div>
                        <div class="form-group">
                            <label >Nama Lengkap</label>
                            <input type="text" class="form-control" name="complete_name" placeholder="masukan nama lengkap" >
                        </div>
                        <div class="form-group">
                            <label >Golongan Darah</label>
                            <select name="gol_darah" class="form-control">
                              <option value="" selected>Pilih Golongan Darah</option>
                              <option value="O">O</option>
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="AB">AB</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Jenis Kelamin</label>
                            <div class="radio">
                              <label><input type="radio" name="jenis_kelamin" value="1" checked>Laki-laki</label>
                            </div>
                            <div class="radio">
                              <label><input type="radio" name="jenis_kelamin" value="2">Perempuan</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label >Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" placeholder="masukan tempat lahir">
                        </div>
                        <div class="form-group">
                            <label >Tanggal Lahir</label>
                            <input type="text" class="form-control" id="dob" name="tanggal_lahir" placeholder="masukan tanggal lahir contoh: 1945-08-17" >
                        </div>
                    
                        <div class="form-group">
                            <label >Status Perkawinan</label>
                             <div class="radio">
                              <label><input type="radio" value="0" name="opt_kawin" value="0" checked>Belum Menikah</label>
                        
                              <label><input type="radio" value="1" name="opt_kawin" value="1">Menikah</label>
                     
                              <label><input type="radio" value="2" name="opt_kawin" value="2">Cerai / Mati</label>
                            </div>
                        </div>
                        <div class="form-group psg">
                            <label >Nama Pasangan</label>
                            <input type="text" class="form-control" name="psg" placeholder="masukan nama pasangan">
                        </div>
                          <div class="form-group">
                            <label >Nama Ayah</label>
                            <input type="text" class="form-control" name="ayah" placeholder="masukan nama pasangan">
                        </div>
                          <div class="form-group">
                            <label >Nama Ibu</label>
                            <input type="text" class="form-control" name="ibu" placeholder="masukan nama pasangan" >
                        </div>
                        <div class="form-group">
                            <label >Alamat</label>
                            <textarea class="form-control" name="alamat" placeholder="masukan alamat"></textarea>
                        </div>
                          <div class="form-group">
                            <label >Provinsi</label>
                                         <input type="text" class="form-control" name="prov" placeholder="masukan Provinsi">
                        </div>
                        <div class="form-group">
                            <label >Kabupaten Kota</label>
                            <input type="text" class="form-control" name="kk" placeholder="masukan Kabupaten Kota">
                        </div>
                        <div class="form-group">
                            <label >Kecamatan</label>
                            <input type="text" class="form-control" name="kc" placeholder="masukan kecamatan">
                        </div>
                        <div class="form-group">
                            <label >Kelurahan</label>
                            <input type="text" class="form-control" name="kl" placeholder="masukan kelurahan">
                        </div>
                        <div class="form-group">
                            <label >Kode Pos</label>
                            <input type="text" class="form-control" name="kpos" placeholder="masukan kode pos">
                        </div>
                        <div class="form-group ">
                            <label >Telephone</label>
                            <input type="text" class="form-control" name="telp" placeholder="masukan nomor telephone">
                        </div>
                        <div class="form-group">
                            <h3>Pendidikan</h3>
                        </div>
                        <div class="form-group">
                            <label class="label-sekolah" >Sekolah Dasar</label>
                            <input type="text" class="form-control nama-sekolah" name="nama_sd" placeholder="masukan nama Sekolah">
                            <input type="text" class="form-control tahun-sekolah" name="tahun_sd" placeholder="masukan tahun lulus sekolah" maxlength="4">
                        </div>
                        <div class="form-group">
                            <label class="label-sekolah" >Sekolah Menengah Pertama</label>
                            <input type="text" class="form-control nama-sekolah" name="nama_smp" placeholder="masukan nama Sekolah">
                            <input type="text" class="form-control tahun-sekolah" name="tahun_smp" placeholder="masukan tahun lulus sekolah" maxlength="4">
                        </div>
                          <div class="form-group">
                            <label class="label-sekolah" >Sekolah Menengah Atas</label>
                            <input type="text" class="form-control nama-sekolah" name="nama_sma" placeholder="masukan nama Sekolah">
                            <input type="text" class="form-control tahun-sekolah" name="tahun_sma" placeholder="masukan tahun lulus sekolah" maxlength="4">
                        </div>
                        <div class="form-group">
                            <label class="label-sekolah" >Perguruan Tinggi</label>
                            <input type="text" class="form-control nama-sekolah" name="nama_pt" placeholder="masukan nama Perguruan Tinggi">
                            <input type="text" class="form-control tahun-sekolah" name="tahun_pt" placeholder="masukan tahun Perguruan Tinggi" maxlength="4">
                        </div>
                       <div class="form-group">
                            <h3>Data Pekerjaan</h3>
                        </div>
                        <div class="form-group">
                            <label >Jenis Pekerjaan</label>
                              <select name="jenis_krj" id="jenis_krj" class="form-control">
                              <option value="0" selected>Tidak Bekerja</option>
                              <option value="1">Pelajar / Mahasiswa</option>
                              <option value="2">Swasta</option>
                              <option value="3">PNS</option>
                              <option value="4">TNI / Polri</option>
                              <option value="5">Wiraswasta</option>
                            </select>
                        </div>
                        <div class="form-group krj">
                            <label >Nama tempat kerja</label>
                            <input type="text" class="form-control" name="nama_kantor" placeholder="masukan nama kantor">
                        </div>
                        <div class="form-group krj">
                            <label >Alamat kantor</label>
                            <textarea class="form-control" name="alamat_kantor" placeholder="masukan alamat kantor"></textarea>
                        </div>

                         <div class="form-group krj">
                            <h3>Pengalaman Kerja (dari yang terakhir)</h3>
                        </div>

                        <div class="form-group krj">
                            <label >1. Nama Instansi / Perusahaan </label>
                            <input type="text" class="form-control" name="nama_kantorx1" placeholder="masukan nama instansi / perusahaan">
                        </div>
                        <div class="form-group krj">
                            <label >Tahun mulai kerja</label>
                            <textarea class="form-control" name="tahun_kerjax1" placeholder="masukan tahun awal kerja"></textarea>
                        </div>
                          <div class="form-group krj">
                            <label >2. Nama Instansi / Perusahaan</label>
                            <input type="text" class="form-control" name="nama_kantorx2" placeholder="masukan nama instansi / perusahaan">
                        </div>
                        <div class="form-group krj">
                            <label >Tahun mulai kerja</label>
                            <textarea class="form-control" name="tahun_kerjax2" placeholder="masukan tahun awal kerja"></textarea>
                        </div>
 
              <div class="form-group krj">
                            <label >3. Nama Instansi / Perusahaan</label>
                            <input type="text" class="form-control" name="nama_kantorx3" placeholder="masukan nama instansi / perusahaan">
                        </div>
                        <div class="form-group krj">
                            <label >Tahun mulai kerja</label>
                            <textarea class="form-control" name="tahun_kerjax3" placeholder="masukan tahun awal kerja"></textarea>
                        </div>
              <div class="form-group krj">
                            <label >4. Nama Instansi / Perusahaan</label>
                            <input type="text" class="form-control" name="nama_kantorx4" placeholder="masukan nama instansi / perusahaan">
                        </div>
                        <div class="form-group krj">
                            <label >Tahun mulai kerja</label>
                            <textarea class="form-control" name="tahun_kerjax4" placeholder="masukan tahun awal kerja"></textarea>
                        </div>
 
 

                        <div class="form-group">
                          <input type="submit" class="btn btn-success pull-right" value="update">
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <script type="text/javascript">
       

        </script>
@endsection
