@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  <h3>Pendaftaran Klub</h3>
                  @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                     <form class="form" method="post" action="{{ url('/member/daftarklubproc')}}">
                        {{ csrf_field() }}
    
                    <div class="form-group">
                      <label>Nama Klub</label>
                      <input type="text" name="nama_klub" class="form-control" placeholder="Masukan Nama Klub">
                    </div>
                    <div class="form-group">
                      <label>Alamat Klub</label>
                      <textarea class="form-control" name="alamat_klub" placeholder="Masukan Alamat Klub"></textarea>
                    </div>

                    <div class="form-group">
                      <label>Nama Ketua</label>
                      <select class="carimember form-control" name="ketua"></select>
                    </div>

                     <div class="form-group">
                      <label>Nama Sekretaris</label>
                      <select class="carimember form-control" name="sekretaris"></select>
                    </div>

                     <div class="form-group">
                      <label>Nama Bendahara</label>
                      <select class="carimember form-control" name="bendahara"></select>
                    </div>


                        <div class="form-group">
                          <input type="submit" class="btn btn-success pull-right" value="Kirim">
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
