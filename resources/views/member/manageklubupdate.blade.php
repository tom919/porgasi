@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Update Klub</h2>
                
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                              @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="form" method="post" action="{{ url('/manageklubupdateproc')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$klub->id}}">
                          <div class="form-group">
                            <label >Nama Klub</label>
                            <input type="text" class="form-control" name="nama_klub" placeholder="masukan Nama Klub" value="{{$klub->nama_klub}}">
                        </div>

                    <div class="form-group">
                      <label>Alamat Klub</label>
                      <textarea class="form-control" name="alamat" placeholder="Masukan Alamat Klub">{{$klub->alamat}}</textarea>
                    </div>


                    <div class="form-group">
                      <label>Ketua</label>
                      <select class="carimember form-control" name="ketua">
                          <option value="{{$klub->id_ketua}}" selected>{{$klub->username_ketua}}</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Sekretaris</label>
                      <select class="carimember form-control" name="sekretaris">
                          <option value="{{$klub->id_sekretaris}}" selected>{{$klub->username_sekretaris}}</option>
                      </select>
                    </div>
                    
                    <div class="form-group">
                      <label>Bendahara</label>
                      <select class="carimember form-control" name="bendahara">
                          <option value="{{$klub->id_bendahara}}" selected>{{$klub->username_bendahara}}</option>
                      </select>
                    </div>
 

                        <div class="form-group">
                          <input type="submit" class="btn btn-success pull-right" value="update">
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
