@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                    <form class="form" method="post" action="#" enctype="multipart/form-data">
                      <h3>Permohonan Pemusnahan Unit</h3>

                       <div class="table-responsive">          
  <table class="table">
    <tbody>
      <tr>
        <td>Jenis Unit</td><td>Senapan Serbu</td>
      </tr>
            <tr>
        <td>Tipe Unit</td><td>M4</td>
      </tr>
      <tr>
        <td>Merek Unit</td><td>Tokyo Marui</td>
      </tr>
      <tr>
        <td>Kode Registrasi</td><td>123/XX/2018</td>
      </tr>
    </tbody>
  </table>
  </div>


                    <div class="form-group">
                      <label>Alasan Pemusnahan</label>
                      <textarea class="form-control">Masukan Alasan Pemusnahan</textarea>
                    </div>
                  
                        <div class="form-group">
                          <input type="submit" class="btn btn-success pull-right" value="Kirim">
                        </div>
                        <br>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
