@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                     <h2>Detail Klub</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <!-- detail utama -->
                     <div class="table-responsive">        
                      <table class="table">
                        <tbody>
                          <tr>
                            <td>Nama Klub</td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Alamat Klub</td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Nama Ketua</td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Nama Sekretaris</td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Nama Bendahara</td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                      </div>
                      <!-- end of detail utama-->

                      <!--- start of anggota -->



                      <!--- end of anggota -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
