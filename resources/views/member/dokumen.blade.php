@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Dokumen Member</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <a href="{{URL::to('/')}}/member/dokumenupload"><button class="btn btn-primary btn-sm">Upload Document</button></a>
                     <div class="table-responsive">
                     <h3>Daftar Dokumen</h3>  
                       @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif        
                      <table class="table">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Jenis Dokumen</th>
                            <th>Preview</th>
                            <th>Tanggal Upload</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($docs as $doc)
                          <tr>
                            <td>{{$doc->id}}</td>
                            <td>{{$doc->category}}</td>
                            <td><img class="zoom" src="{{asset('member/dokumen')}}/{{$doc->document_name}}"></td>
                            <td>{{$doc->created_at}}</td>
                            <td><b>{{$doc->status}}</b></td>
                            <td><a href="{{URL::to('/')}}/member/dokumendelete/{{$doc->id}}"><button class="btn btn-danger btn-sm" onclick="return confirm('anda yakin hapus dokumen ini?')">Hapus</button></a></td>
                          </tr>
                            @endforeach
                        </tbody>
                      </table>
                      </div>
                    <br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
