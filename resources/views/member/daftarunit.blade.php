@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Daftar Unit</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                         @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                     <div class="table-responsive">
                           
                      <table class="table">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Preview</th>
                            <th>Kode Registrasi</th>
                            <th>Kaliber</th>
                            <th>Tipe</th>
                            <th>Merek</th>
                            <th>Mekanisme</th>
                            <th>Tanggal Registrasi</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($units as $unit)
                          <tr>
                            <td>{{$unit->id}}</td>
                            <td><img class="zoom" src="{{asset('member/unit')}}/{{$unit->foto}}"></td>
                            <td>{{$unit->register_id}}</td>
                            <td>{{$unit->kaliber}}</td>
                            <td>{{$unit->unit_type}}</td>
                            <td>{{$unit->merek}}</td>
                            <td>{{$unit->mekanisme}}</td>
                            <td>{{$unit->created_at}}</td>
                            <td><b>{{$unit->status}}</b></td>
                            <td>
                              @if($unit->status_id == '0' || $unit->status_id == '4')
                              <a href="{{URL::to('/')}}/member/unitdeleteproc/{{$unit->id}}"><button class="btn btn-danger btn-sm" onclick="return confirm('anda yakin hapus unit ini?')">Hapus</button></a>
                              @endif
                            </td>
                          </tr>
                            @endforeach
                        </tbody>
                      </table>
                      </div>
                    <br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
