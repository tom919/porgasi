@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Home</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><div class="welcome-notif">selamat datang {{Auth::user()->username}}</div></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
              
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="welcome-logo">
                   
                    <br>
                     <img src="{{ asset('img/logo.png')}}" class="logo-internal" >
                    </div>
                    <br>
                    <!--
                      <h3>Grade B</h3>
                    <br>
                     <div class="clearfix"></div>
                      <div class="table-responsive">        
                      <table class="table">
                        <tbody>
                          <tr>
                            <td>Profil</td>
                            <td>100% </td>
                            <td width="30%">
                             
                        <div class="progress progress_sm" style="width: 100%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="100" aria-valuenow="100" style="width: 100%;"></div>
                        </div>
                            <td>Data Lengkap</td>
                          </tr>
                          <tr>
                            <td>Dokumen</td>
                            <td>50% </td>
                            <td width="30%">
                           <div class="progress progress_sm" style="width: 100%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50" aria-valuenow="50" style="width: 100%;"></div>
                        </div>
                            </td>
                            <td>SKCK Belum Ada</td>
                          </tr>
                          <tr>
                            <td>Test Psikologi</td>
                            <td>40% </td>
                            <td width="30%">
                         <div class="progress progress_sm" style="width: 100%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="40" aria-valuenow="40" style="width: 100%;"></div>
                        </div>
                            </td>
                            <td>Hasil Psikotest masih dibawah ketentuan</td>
                          </tr>
                                         <tr>
                            <td>Test Wawasan Kebangsaan</td>
                            <td>80% </td>
                            <td width="30%">
                         <div class="progress progress_sm" style="width: 100%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="80" aria-valuenow="80" style="width: 100%;"></div>
                        </div>
                            </td>
                            <td>Hasil Test Wawasan kebangsaan sudah memenuhi kriteria</td>
                          </tr>
                          <tr>
                            <td>Registrasi Unit</td>
                            <td>70% </td>
                            <td width="30%">
                         <div class="progress progress_sm" style="width: 100%;">
                          <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="70" aria-valuenow="70" style="width: 100%;"></div>
                        </div>
                            </td>
                            <td>3 dari 10 unit belum di registrasi</td>
                          </tr>
                        </tbody>
                      </table>
                      </div>
                    -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
