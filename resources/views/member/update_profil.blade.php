@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Update Profil Akun</h2>



                     @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                              @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="form" method="post" action="{{ url('/member/updateprofilproc')}}">
                        {{ csrf_field() }}
                          <div class="form-group">
                            <label >NIK</label>
                            <input type="text" class="form-control" name="NIK" placeholder="masukan NIK" value="{{$user->NIK}}">
                        </div>
                        <div class="form-group">
                            <label >Nama Lengkap</label>
                            <input type="text" class="form-control" name="complete_name" placeholder="masukan nama lengkap" value="{{$user->complete_name}}">
                        </div>
                        <div class="form-group">
                            <label >Golongan Darah</label>
                            <select name="gol_darah" class="form-control">
                                 <option value="O"  @if($user->gol_darah=="O") selected @endif>O</option>
                                <option value="A"  @if($user->gol_darah=="A") selected @endif>A</option>
                                <option value="B" @if($user->gol_darah=="B") selected @endif>B</option>  
                                <option value="AB" @if($user->gol_darah=="AB") selected @endif>AB</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Jenis Kelamin</label>
                            <div class="radio">
                              <label><input type="radio" name="jenis_kelamin" value="1" {{ $user->jenis_kelamin_id == '1' ? 'checked' : ''}}>Laki-laki</label>
                            </div>
                            <div class="radio">
                              <label><input type="radio" name="jenis_kelamin" value="2" {{ $user->jenis_kelamin_id == '2' ? 'checked' : ''}}>Perempuan</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label >Tempat Lahir</label>
                            <input type="text" class="form-control" name="tempat_lahir" placeholder="masukan tempat lahir" value="{{$user->tempat_lahir}}">
                        </div>
                        <div class="form-group">
                            <label >Tanggal Lahir</label>
                            <input type="text" class="form-control" id="dob" name="tanggal_lahir" placeholder="masukan tanggal lahir contoh: 1945-08-17" value="{{$user->tanggal_lahir}}">
                        </div>
                    
                        <div class="form-group">
                            <label >Status Perkawinan</label>
                             <div class="radio">
                              <label><input type="radio" value="0" name="opt_kawin" value="0" {{ $user->status_perkawinan_id == '0' ? 'checked' : ''}}>Belum Menikah</label>
                        
                              <label><input type="radio" value="1" name="opt_kawin" value="1" {{ $user->status_perkawinan_id == '1' ? 'checked' : ''}}>Menikah</label>
                     
                              <label><input type="radio" value="2" name="opt_kawin" value="2" {{ $user->status_perkawinan_id == '2' ? 'checked' : ''}}>Cerai / Mati</label>
                            </div>
                        </div>
                        <div class="form-group psg">
                            <label >Nama Pasangan</label>
                            <input type="text" class="form-control" name="psg" placeholder="masukan nama pasangan" {{$user->nama_pasangan}}>
                        </div>
                          <div class="form-group">
                            <label >Nama Ayah</label>
                            <input type="text" class="form-control" name="ayah" placeholder="masukan nama pasangan" value="{{$user->nama_ayah}}">
                        </div>
                          <div class="form-group">
                            <label >Nama Ibu</label>
                            <input type="text" class="form-control" name="ibu" placeholder="masukan nama pasangan" value="{{$user->nama_ibu}}">
                        </div>
                        <div class="form-group">
                            <label >Alamat</label>
                            <textarea class="form-control" name="alamat" placeholder="masukan alamat">{{$user->alamat}}</textarea>
                        </div>
                          <div class="form-group">
                            <label >Provinsi</label>
                                         <input type="text" class="form-control" name="prov" placeholder="masukan Provinsi" value="{{$user->provinsi}}">
                        </div>
                        <div class="form-group">
                            <label >Kabupaten Kota</label>
                            <input type="text" class="form-control" name="kk" placeholder="masukan Kabupaten Kota" value="{{$user->kabupatenkota}}">
                        </div>
                        <div class="form-group">
                            <label >Kecamatan</label>
                            <input type="text" class="form-control" name="kc" placeholder="masukan kecamatan" value="{{$user->kecamatan}}">
                        </div>
                        <div class="form-group">
                            <label >Kelurahan</label>
                            <input type="text" class="form-control" name="kl" placeholder="masukan kelurahan" value="kelurahan">
                        </div>
                        <div class="form-group">
                            <label >Kode Pos</label>
                            <input type="text" class="form-control" name="kpos" placeholder="masukan kode pos" value="{{$user->kode_pos}}">
                        </div>
                        <div class="form-group ">
                            <label >Telephone</label>
                            <input type="text" class="form-control" name="telp" placeholder="masukan nomor telephone" value="{{$user->telp}}">
                        </div>
                        <div class="form-group">
                            <h3>Pendidikan</h3>
                        </div>
                        <div class="form-group">
                            <label class="label-sekolah" >Sekolah Dasar</label>
                            <input type="text" class="form-control nama-sekolah" name="nama_sd" placeholder="masukan nama Sekolah" value="{{$user->pendidikan_sd}}">
                            <input type="text" class="form-control tahun-sekolah" name="tahun_sd" placeholder="masukan tahun lulus sekolah" maxlength="4" value="{{$user->thn_pendidikan_sd}}">
                        </div>
                        <div class="form-group">
                            <label class="label-sekolah" >Sekolah Menengah Pertama</label>
                            <input type="text" class="form-control nama-sekolah" name="nama_smp" placeholder="masukan nama Sekolah" value="{{$user->pendidikan_smp}}">
                            <input type="text" class="form-control tahun-sekolah" name="tahun_smp" placeholder="masukan tahun lulus sekolah" maxlength="4" value="{{$user->thn_pendidikan_smp}}">
                        </div>
                          <div class="form-group">
                            <label class="label-sekolah" >Sekolah Menengah Atas</label>
                            <input type="text" class="form-control nama-sekolah" name="nama_sma" placeholder="masukan nama Sekolah" value="{{$user->pendidikan_sma}}">
                            <input type="text" class="form-control tahun-sekolah" name="tahun_sma" placeholder="masukan tahun lulus sekolah" maxlength="4" value="{{$user->thn_pendidikan_sma}}">
                        </div>
                        <div class="form-group">
                            <label class="label-sekolah" >Perguruan Tinggi</label>
                            <input type="text" class="form-control nama-sekolah" name="nama_pt" placeholder="masukan nama Perguruan Tinggi" value="{{$user->pendidikan_sarjana}}">
                            <input type="text" class="form-control tahun-sekolah" name="tahun_pt" placeholder="masukan tahun Perguruan Tinggi" maxlength="4" value="{{$user->thn_pendidikan_sarjana}}">
                        </div>
                       <div class="form-group">
                            <h3>Data Pekerjaan</h3>
                        </div>
                        <div class="form-group">
                            <label >Jenis Pekerjaan</label>
                              <select name="jenis_krj" id="jenis_krj" class="form-control">
                                <option value="0"  @if($user->jenis_pekerjaan_id=="0") selected @endif>Tidak Bekerja</option>
                                <option value="1"  @if($user->jenis_pekerjaan_id=="1") selected @endif>Pelajar / Mahasiswa</option>
                                <option value="2" @if($user->jenis_pekerjaan_id=="2") selected @endif>Swasta</option>  
                                <option value="3" @if($user->jenis_pekerjaan_id=="3") selected @endif>PNS</option>
                                <option value="4" @if($user->jenis_pekerjaan_id=="4") selected @endif>TNI / Polri</option>  
                                <option value="5" @if($user->jenis_pekerjaan_id=="5") selected @endif>Wiraswasta</option>
                            </select>
                        </div>
                        <div class="form-group krj">
                            <label >Nama tempat kerja</label>
                            <input type="text" class="form-control" name="nama_kantor" placeholder="masukan nama kantor">
                        </div>
                        <div class="form-group krj">
                            <label >Alamat kantor</label>
                            <textarea class="form-control" name="alamat_kantor" placeholder="masukan alamat kantor"></textarea>
                        </div>

                         <div class="form-group krj">
                            <h3>Pengalaman Kerja (dari yang terakhir)</h3>
                        </div>

                        <div class="form-group krj">
                            <label >1. Nama Instansi / Perusahaan </label>
                            <input type="text" class="form-control" name="nama_kantorx1" placeholder="masukan nama instansi / perusahaan">
                        </div>
                        <div class="form-group krj">
                            <label >Tahun mulai kerja</label>
                            <textarea class="form-control" name="tahun_kerjax1" placeholder="masukan tahun awal kerja"></textarea>
                        </div>
                          <div class="form-group krj">
                            <label >2. Nama Instansi / Perusahaan</label>
                            <input type="text" class="form-control" name="nama_kantorx2" placeholder="masukan nama instansi / perusahaan">
                        </div>
                        <div class="form-group krj">
                            <label >Tahun mulai kerja</label>
                            <textarea class="form-control" name="tahun_kerjax2" placeholder="masukan tahun awal kerja"></textarea>
                        </div>
 
              <div class="form-group krj">
                            <label >3. Nama Instansi / Perusahaan</label>
                            <input type="text" class="form-control" name="nama_kantorx3" placeholder="masukan nama instansi / perusahaan">
                        </div>
                        <div class="form-group krj">
                            <label >Tahun mulai kerja</label>
                            <textarea class="form-control" name="tahun_kerjax3" placeholder="masukan tahun awal kerja"></textarea>
                        </div>
              <div class="form-group krj">
                            <label >4. Nama Instansi / Perusahaan</label>
                            <input type="text" class="form-control" name="nama_kantorx4" placeholder="masukan nama instansi / perusahaan">
                        </div>
                        <div class="form-group krj">
                            <label >Tahun mulai kerja</label>
                            <textarea class="form-control" name="tahun_kerjax4" placeholder="masukan tahun awal kerja"></textarea>
                        </div>
 
 

                        <div class="form-group">
                          <input type="submit" class="btn btn-success pull-right" value="update">
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
@endsection
