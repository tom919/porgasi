@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="form" method="post" action="{{ url('/member/pendaftaranunitproc')}}" enctype="multipart/form-data">
                      <h3>Registrasi Unit</h3>
                      {{ csrf_field() }}
                    <div class="form-group">
                        <label>Kaliber</label>
                        <select name="kaliber_unit" class="form-control">
                          <option selected value="">Pilih kaliber</option>
                          <option value="6">6 mm</option>
                          <option value="4.5">4.5 mm</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <label>Tipe Unit</label>
                      <select class="form-control" name="tipe_unit">
                        <option selected value="">Pilih Tipe</option>
                        <option value="Senapan Serbu">Senapan Serbu</option>
                        <option value="Pistol">Pistol</option>
                        <option value="Senapan Runduk">Senapan Runduk</option>
                        <option value="SMG">SMG</option>
                        <option value="Senapan Mesin">Senapan Mesin</option>
                        <option value="Lain-lain">Lain-lain</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Merek</label>
                      <input type="text" name="merek_unit" class="form-control" placeholder="Masukan merek unit">
                    </div>
                    <div class="form-group">
                        <label>Mekanisme</label>
                        <select name="mekanisme_unit" class="form-control">
                          <option selected value="">Pilih mekanisme</option>
                          <option value="Spring">Spring</option>
                          <option value="AEG">AEG</option>
                          <option value="GBB">GBB</option>
                        </select>
                    </div>

                    <div class="form-group">
                      <label>Pilih Foto</label> *Ukuran File maksimal 1 Mb
                      <input type="file" name="foto_unit" class="form-control">
                    </div>
                        <div class="form-group">
                          <input type="submit" class="btn btn-success pull-right" value="Daftarkan">
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
