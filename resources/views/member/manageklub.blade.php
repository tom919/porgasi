@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                     <h2>Manage Klub</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="table-responsive">        
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Id</th>
                            <th>Nama Klub</th>
                            <th>Status</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($klub as $k)
                          <tr>
                            <td>{{$k->id}}</td>
                            <td>{{$k->nama_klub}}</td>
                            <td>{{$k->status}}</td>
                            <td><a href="{{URL::to('/')}}/member/manageklubdetail/{{$k->id}}"><button class="btn btn-sm btn-default">Lihat</button></a><a href="{{URL::to('/')}}/manageklubupdate/{{$k->id}}"><button class="btn btn-sm btn-default">Edit</button></a><a href="{{URL::to('/')}}/member/manageklubdelete/{{$k->id}}"><button class="btn btn-danger btn-sm" onclick="return confirm('anda yakin hapus dokumen ini?')">Hapus</button></a></td>
                          </tr>
                            @endforeach
                        </tbody>
                      </table>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
