@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Dokumen Member</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                    
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="form" method="post" action="{{ url('/member/dokumenuploadproc')}}" enctype="multipart/form-data">
                      {{ csrf_field() }}
                      <h3>Upload Dokumen</h3>
                    <div class="form-group">
                      <label>Jenis Dokumen</label>
                      <select class="form-control" name="jns_dok">
                        <option selected>Pilih Jenis Dokumen</option>
                        <option value="1">KTP</option>
                        <option value="2">SIM</option>
                        <option value="3">SKCK</option>
                        <option value="4">Kartu Keluarga</option>
                        <option value="5">Surat Rekomendasi</option>
                        <option value="6">Foto</option>
                        <option value="7">Lain-lain</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Pilih Dokumen</label>
                      <input type="file" name="foto" class="form-control">
                    </div>
                        <div class="form-group">
                          <button type="submit" class="btn btn-success pull-right">Upload &nbsp;<span class="glyphicon glyphicon-upload"></span></button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
