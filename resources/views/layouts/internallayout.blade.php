<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Porgasi Jateng</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('img/logo.png')}}"/>
    <!-- Scripts -->
    <!--<script src="{{ asset('js/app.js') }}" defer></script>-->

    <!-- Fonts -->
    <!--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">-->


    <!-- Custom Theme Style -->
    <!-- Bootstrap -->
    <link href="{{ asset('/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{ asset('/vendors/animate.css/animate.min.css') }}" rel="stylesheet">

     <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <!-- Styles -->
   <link href="{{ asset('css/internalcustomstyle.css') }}" rel="stylesheet">
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="{{ asset('js/jqueryui/jquery-ui.css') }}" rel="stylesheet" type="text/css"/>  
     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
</head>
 <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{URL::to('/')}}/home" class="site_title"><img src="{{ asset('img/logo.png')}}" class="internal-logo"> <span>{{ env('APP_NAME') }}</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
               <!--     @if(Auth::user()->role_id==0)
                    <div>
                        <img src="{{ asset('img/pp/root.jpg') }}" alt="..." class="img-circle profile_img">
                    </div>
                    @else
                    <div>
                         <img src="#" alt="..." class="img-circle profile_img">
                    </div>
                    @endif-->

               
              </div>
              <div class="profile_info">
                
                    @if(Auth::user()->role_id==0)
                        <h2>Super Admin</h2>
                    @elseif(Auth::user()->role_id==1)
                        <h2>Admin</h2>
                    @elseif(Auth::user()->role_id==2)
                        <h2>Polisi</h2>
                    @elseif(Auth::user()->role_id==3)
                        <h2>Member</h2>
                    @endif
                <br>
                <h4>{{ Auth::user()->username }}</h4>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{URL::to('/')}}/setting">Setting Akun</a></li>
                    </ul>
                  </li>

              @if(Auth::user()->role_id==0)
                <div class="menu_section">
                <h3>Menu Super Admin</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-user"></i> Management Admin <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">Management Admin</a></li>
                      <li><a href="projects.html">Log Sistem</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
               <div class="menu_section">
                <h3>Menu Admin</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-users"></i> Management User <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="projects.html">Management Polisi</a></li>
                      <li><a href="#">Management User</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-book"></i> Management Konten <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">Halaman</a></li>
                      <li><a href="projects.html">News</a></li>
                      <li><a href="project_detail.html">Galeri</a></li>
                      <li><a href="project_detail.html">Kontak</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-book"></i> Management Klub <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">Halaman</a></li>
                      <li><a href="projects.html">News</a></li>
                      <li><a href="project_detail.html">Galeri</a></li>
                      <li><a href="project_detail.html">Kontak</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            
            
                @elseif(Auth::user()->role_id==1)
                 <div class="menu_section">
                <h3>Menu Admin</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-users"></i> Management User <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <!--<li><a href="{{URL::to('/')}}/admin/managementpolisi">Management Polisi</a></li>-->
                      <li><a href="{{URL::to('/')}}/admin/managementmember">Management User Member</a></li>
                       <li><a href="#">Management User Non Member</a></li>
                    </ul>
                  </li>
                <li><a href="{{URL::to('/')}}/admin/managementclub"><i class="fa fa-book"></i> Management Klub</a>
                  </li>

                   <li><a href="{{URL::to('/')}}/admin/daftarunit"> <span class="glyphicon glyphicon-screenshot"></span> Unit</a>
                    
                  </li>
                  <!--
                  <li><a><i class="fa fa-book"></i> Management Konten <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">Halaman</a></li>
                      <li><a href="projects.html">News</a></li>
                      <li><a href="project_detail.html">Galeri</a></li>
                      <li><a href="project_detail.html">Kontak</a></li>
                    </ul>
                  </li>


                <li><a><i class="fa fa-book"></i> Management Test <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">Test Psikologi</a></li>
                      <li><a href="projects.html">Test Kebangsaan</a></li>
                      <li><a href="project_detail.html">Test Pengetahuan Unit</a></li>
                    </ul>
                  </li>
                        <li><a><i class="fa fa-book"></i> Perijinan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">Perijinan Unit</a></li>
                      <li><a href="projects.html">Perijinan Angkut Unit</a></li>
                      <li><a href="project_detail.html">Perijinan Kegiatan</a></li>
                    </ul>
                  </li>
                -->
                </ul>
              </div>
              @elseif(Auth::user()->role_id==2)
                <div class="menu_section">
                <h3>Menu Polisi</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Perijinan <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="projects.html">Perijinan Unit</a></li>
                      <li><a href="project_detail.html">Perijinan Kegiatan</a></li>
                      <li><a href="project_detail.html">Perijinan Angkut Unit </a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-book"></i> Data Anggota Porgasi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">List Anggota</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-book"></i> Data Klub Porgasi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">List Klub</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
                @elseif(Auth::user()->role_id==3)
                  @if(Auth::user()->flag==2)
                 <div class="menu_section">
                <h3>Menu Member</h3>
                <ul class="nav side-menu">
                  <li><a href="{{URL::to('/')}}/member/profil"><i class="fa fa-users"></i> Profil </a>
                  </li>
                  <li><a href="{{URL::to('/')}}/member/dokumenlist"><i class="fa fa-book"></i> Dokumen </a>
                  </li>
               <!--   <li><a href="#"><i class="fa fa-pencil-square-o"></i> Test Psikologi </a>
                  </li>
                  <li><a href="#"><i class="fa fa-flag"></i> Test Wawasan Kebangsaan </a>
                  </li>-->
                  <li><a><i class="fa fa-users"></i>&nbsp; Klub <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{URL::to('/')}}/member/daftarklub">Pendaftaran Klub</a></li>
                      <li><a href="{{URL::to('/')}}/member/manageklub">Management Klub</a></li>
                    </ul>
                  </li>
                  
                  <li><a> <span class="glyphicon glyphicon-screenshot"></span> Unit <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{URL::to('/')}}/member/pendaftaranunit">Pendaftaran Unit</a></li>
                      <li><a href="{{URL::to('/')}}/member/daftarunit">Daftar Unit</a></li>

                    <!--</ul>-->
                  </li>
                </ul>
              </div>
                @else
                  <div>
                  </div>
                @endif
                    @endif
                 
                </ul>
              </div>


            </div>
            <!-- /sidebar menu -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    @if(Auth::user()->role_id==0)
                    
                        <img src="{{ asset('img/pp/root.jpg') }}" alt="..." class="img-circle right_profile_img">
                    @else
                         <img src="{{ asset('img/pp/root.jpg') }}" alt="..." class="img-circle right_profile_img">
                    @endif

                    {{Auth::user()->username}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <!--<li><a href="javascript:;"> Setting Akun</a></li>-->
                    <li>  
                      <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout <i class="fa fa-sign-out" style="position: right"></i> </a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </li>
                  </ul>
                </li>

           
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
            @yield('content')
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Porgasi Jawa Tengah
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
</body>
    <script src="{{ asset('js/jquery224.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script> 
     <script src="{{ asset('vendors/fastclick/lib/fastclick.js') }}"></script>
     <script src="{{ asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
      <script src="{{ asset('vendors/nprogress/nprogress.js') }}"></script>
      <script src="{{ asset('js/custom.min.js') }}"></script> 
   <script src="{{ asset('js/jqueryui/jquery-ui.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

  <script type="text/javascript">
       $(function() {
                $("#dob").datepicker({ dateFormat: "yy-mm-dd" }).val()
               $("#startdate").datepicker({ dateFormat: "yy-mm-dd" }).val()
               $("#enddate").datepicker({ dateFormat: "yy-mm-dd" }).val()
       });

   </script> 
           <script type="text/javascript">
            
        $(document).ready(function(){
          $(".psg").hide();
          $(".krj").hide();
          $('input:radio[name="opt_kawin"]').change(function(){
                     if($(this).val()=="1")
                     {
                       $(".psg").show();
                     }
                     else
                     {
                         $(".psg").hide();
                     }
            });

                    $("#jenis_krj").change(function(){
                     if($(this).val()=="0" || $(this).val()=="1" )
                     {
                       $(".krj").hide();
                     }
                     else
                     {
                         $(".krj").show();
                     }
                    });


            
             
          
          });




        </script> 


  <script type="text/javascript">
  $('.carimember').select2({
    placeholder: 'Cari...',
    ajax: {
      url: '../carimember',
      dataType: 'json',
      minimumInputLength: 3,
      delay: 250,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.username,
              id: item.id
            }
          })
        };
      },
      cache: false
    }
  });

</script>
</html>
