<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Porgasi Jateng</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('img/logo.png')}}"/>
    <!-- Scripts -->
    <!--<script src="{{ asset('js/app.js') }}" defer></script>-->

    <!-- Fonts -->
    <!--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">-->


    <!-- Custom Theme Style -->
    <!-- Bootstrap -->
    <link href="{{ asset('/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{ asset('/vendors/animate.css/animate.min.css') }}" rel="stylesheet">

     <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <!-- Styles -->
    <!--<link href="{{ asset('css/internalstyle.css') }}" rel="stylesheet">-->
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="{{ asset('js/jqueryui/jquery-ui.css') }}" rel="stylesheet" type="text/css"/>  
   <script src="{{ asset('js/jquery.min.js') }}"></script>  
   <script src="{{ asset('js/jqueryui/jquery-ui.js') }}"></script>  
</head>
<body>
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
  <script type="text/javascript">
       $(function() {
                $("#dob").datepicker({ dateFormat: "yy-mm-dd" }).val()
               $("#startdate").datepicker({ dateFormat: "yy-mm-dd" }).val()
               $("#enddate").datepicker({ dateFormat: "yy-mm-dd" }).val()
       });

   </script>  
</html>
