@extends('layouts.internallayout')

@section('content')
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3></h3>
              </div>

              <div class="title_right">

              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  
                    <form class="form" method="post" action="#" enctype="multipart/form-data">
                      <h3>Update Setting</h3>

                    <div class="form-group">
                      <label>Username</label>
                     <input type="text" name="sername" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                     <input type="text" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Password Lama</label>
                     <input type="password" name="old_password" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Password Baru</label>
                     <input type="password" name="password" class="form-control">
                    </div>
                      <div class="form-group">
                      <label>Masukan Ulang Password Baru</label>
                     <input type="text" name="repassword" class="form-control">
                    </div>
                  
                        <div class="form-group">
                          <input type="submit" class="btn btn-success pull-right" value="Update">
                        </div>
                        <br>
                    </form>
                    <br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

@endsection
