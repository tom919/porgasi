<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>Porgasi Jateng</title>
             

        
        <!-- Styles -->
        <!-- Styles -->
            <link href="{{ asset('css/style.css') }}" rel="stylesheet">
             <link href="{{ asset('bs/css/bootstrap.min.css') }}" rel="stylesheet">
      
            
    </head>
    <body>


               <div class="main">
                    <img src="{{ asset('img/logo.png')}}" class="logo">
                    <h1>PORGASI JATENG</h1>
                <h3>Sistem Informasi </h3>
                  @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}"><button class="btn btn-outline-warning">Login</button></a>
                      <!--  <a href="{{ route('register') }}"><button class="btn btn-outline-warning">Register</button></a>-->
                    @endauth
                </div>
                @endif
                </div>
           

    </body>
      <!-- javascript -->
            <script src="{{ asset('js/jquery.min.js') }}"></script>
          <script src="{{ asset('bs/js/bootstrap.min.js') }}"></script>
</html>
