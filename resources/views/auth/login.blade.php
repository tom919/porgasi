@extends('layouts.loginlayout')

@section('content')
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
             <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf
              <h1>Login</h1>
              <div>
                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
              </div>
              <div>
                 <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
              </div>
              <div>
                 <input id="password" type="text" class="form-control" name="konfirmasinomoracak" placeholder="Masukan angka unik" >
                 @if ($errors->has('konfirmasinomoracak'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Nomor Acak Tidak Sama</strong>
                                    </span>
                                @endif
           
              </div>
                     <div class="uniq form-control">

                    <?php
                      $randvalue =  rand(100000,900000);
                      echo "<h3>".$randvalue."</h3>";
                    ?>

                  </div>
                  <input type="hidden" value="<?php echo $randvalue; ?>" name="nomoracak">

              <div>
                
                 <button type="submit" class="btn btn-default custom-login-btn">
                                    {{ __('Login') }}
                                </button>
                 <!--<a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}-->
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Pengguna Baru?
                  <a href="#signup" class="to_register"> Daftar </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="{{ asset('img/logo.png')}}" class="cst-login-logo">&nbsp;Porgasi Jateng</h1>
                  <p>©2018 All Rights Reserved.</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <!--- registration form-->
        <div id="register" class="animate form registration_form">
          <section class="login_content">
             <p class="change_link">Sudah memiliki Akun ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>
            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
				@csrf
              <h1>Pendaftaran Member</h1>
              <div>
                <input type="text" class="form-control" name="username" placeholder="Username"  />
              </div>
              <div>
                <input type="email" class="form-control" name="email" placeholder="Email"  />
              </div>
              <div>
                <input type="password" class="form-control" name="password"  placeholder="Password" />
              </div>
              <div>
                <input type="password" class="form-control" name="repassword"  placeholder="Konfirmasi Password"  />
              </div>
            

             
            <div>
              <br>
              <div>
               <button type="submit" class="btn btn-default custom-login-btn">
                                   Daftar
                                </button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
               

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="{{ asset('img/logo.png')}}" class="cst-login-logo">&nbsp;Porgasi Jateng</h1>
                  <p>©2018 All Rights Reserved.</p>
                </div>
              </div>
            </form>
          
          </section>
        </div>

        <!-- end of registration form-->
      </div>
    </div>
@endsection
