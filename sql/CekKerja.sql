DELIMITER //

DROP FUNCTION IF EXISTS CekKerja;

CREATE FUNCTION CekKerja ( id INT )
RETURNS varchar(30) DETERMINISTIC

BEGIN

   DECLARE JnsKerja varchar(20);

   IF id = 0 THEN
      SET JnsKerja = 'Tidak Bekerja';

   ELSEIF id = 1 THEN
      SET JnsKerja = 'Pelajar / Mahasiswa';
			
	 ELSEIF id = 2 THEN
      SET JnsKerja = 'Swasta';

	  ELSEIF id = 3 THEN
      SET JnsKerja = 'PNS';
		
		ELSEIF id = 4 THEN
      SET JnsKerja = 'TNI / POLRI';
			
		ELSEIF id = 5 THEN
      SET JnsKerja = 'Wiraswasta';
   
	 ELSE
      SET JnsKerja = 'Tidak Terdaftar';

   END IF;

   RETURN JnsKerja;

END; //

DELIMITER ;