
DELIMITER //

DROP PROCEDURE IF EXISTS AdmMemberList;

CREATE PROCEDURE AdmMemberList ()
 BEGIN
		
		
		  SELECT
					users.id AS id,
					users.username AS username,
					users.email AS email,
					users.role_id AS role_id,
					CekRole(users.role_id) AS user_role,
					tbl_member.complete_name AS complete_name,
					tbl_member.status AS status_id,
					CekMemberStatus(tbl_member.status) AS status
					
					
		 FROM users
		 LEFT JOIN tbl_member
					ON users.id = tbl_member.user_id
		 WHERE users.role_id = 3
		 ORDER BY users.id	;
				
		
 END;
//

DELIMITER ;

/*admin user list*/

/*admin user detail */
DELIMITER //

DROP PROCEDURE IF EXISTS AdmMemberDetail;

CREATE PROCEDURE AdmMemberDetail (IN idx INT)
 BEGIN
		
		
		  SELECT
					users.id AS id,
					users.username AS username,
					CekRole(users.role_id) AS role,
					tbl_member.NIK AS NIK,
					tbl_member.user_id AS user_id,
					tbl_member.user_register_id AS user_register_id,
					tbl_member.complete_name AS complete_name,
					tbl_member.gol_darah AS gol_darah,
					tbl_member.jenis_kelamin AS jenis_kelamin_id,
					CekGender(tbl_member.jenis_kelamin) AS jenis_kelamin,
					tbl_member.tempat_lahir AS tempat_lahir,
					tbl_member.tanggal_lahir AS tanggal_lahir,
					tbl_member.nama_ayah AS nama_ayah,
					tbl_member.nama_ibu AS nama_ibu,
					tbl_member.nama_pasangan AS nama_pasangan,
					tbl_member.status_perkawinan AS status_perkawinan_id,
					CekStatNikah(tbl_member.status_perkawinan) AS status_perkawinan,
					tbl_member.alamat AS alamat,
					tbl_member.provinsi AS provinsi,
					tbl_member.kabupatenkota AS kabupatenkota,
					tbl_member.kecamatan AS kecamatan,
					tbl_member.kelurahan AS kelurahan,
					tbl_member.kode_pos AS kode_pos,
					tbl_member.jenis_pekerjaan AS jenis_pekerjaan_id,
					CekKerja(tbl_member.jenis_pekerjaan) AS jenis_pekerjaan,
					tbl_member.nama_kantor AS nama_kantor,
					tbl_member.alamat_kantor AS alamat_kantor,
					tbl_member.telp AS telp,
					users.email AS email,
					tbl_member.pendidikan_sd AS pendidikan_sd,
					tbl_member.thn_pendidikan_sd AS thn_pendidikan_sd,
					tbl_member.pendidikan_smp AS pendidikan_smp,
					tbl_member.thn_pendidikan_smp AS thn_pendidikan_smp,
					tbl_member.pendidikan_sma AS pendidikan_sma,
					tbl_member.thn_pendidikan_sma AS thn_pendidikan_sma,
					tbl_member.pendidikan_sarjana AS pendidikan_sarjana,
					tbl_member.thn_pendidikan_sarjana AS thn_pendidikan_sarjana,
					tbl_member.pengalaman_kerja1 AS pengalaman_kerja1,
					tbl_member.thn_pengalaman_kerja1 AS thn_pengalaman_kerja1,
					tbl_member.pengalaman_kerja2 AS pengalaman_kerja2,
					tbl_member.thn_pengalaman_kerja2 AS thn_pengalaman_kerja2,
					tbl_member.pengalaman_kerja3 AS pengalaman_kerja3,
					tbl_member.thn_pengalaman_kerja3 AS thn_pengalaman_kerja3,
					tbl_member.pengalaman_kerja4 AS pengalaman_kerja4,
					tbl_member.thn_pengalaman_kerja4 AS thn_pengalaman_kerja4,
					CekMemberStatus(tbl_member.status) AS status
					
					
					
		 FROM users
		 LEFT JOIN tbl_member
					ON users.id = tbl_member.user_id
		WHERE users.id = idx	;
				
		
 END;
//

DELIMITER ;

/*admin user detail*/

/*admin member update registration code*/

DELIMITER //

DROP PROCEDURE IF EXISTS AdmUpdateMemberRegCode;

CREATE PROCEDURE AdmUpdateMemberRegCode (IN id_u INT, IN member_reg_code VARCHAR(255), OUT flag VARCHAR(10))
 BEGIN
		
		START TRANSACTION;
		
		UPDATE tbl_member 
		SET
			user_register_id = member_reg_code

		WHERE
				user_id = id_u; 

		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//

DELIMITER ;
/* end of admin member update registration code */

/*admin member update status*/

DELIMITER //

DROP PROCEDURE IF EXISTS AdmUpdateMemberStatus;

CREATE PROCEDURE AdmUpdateMemberStatus (IN id_u INT, IN member_status VARCHAR(255), OUT flag VARCHAR(10))
 BEGIN
		
		DECLARE flax INT;
		
		START TRANSACTION;
		
		UPDATE tbl_member 
		SET
			status = member_status

		WHERE
				user_id = id_u; 
				
		
		
		IF member_status = 1 THEN
      SET flax = 2;	
		ELSE
      SET flax = 0;
   END IF;


		UPDATE users
		
		SET 
			flag = flax
		
		WHERE
				id = id_u; 

		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//

DELIMITER ;
/* end of admin member update status */

/*admin member update dokumen status*/

DELIMITER //

DROP PROCEDURE IF EXISTS AdmUpdateMemberDocStatus;

CREATE PROCEDURE AdmUpdateMemberDocStatus (IN id_u INT, IN dok_status VARCHAR(255), OUT flag VARCHAR(10))
 BEGIN
		
		START TRANSACTION;
		
		UPDATE tbl_document 
		SET
			status = dok_status

		WHERE
				id = id_u; 

		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//

DELIMITER ;
/* end of admin member update dokumen status */


/*admin search user */
DELIMITER //

DROP PROCEDURE IF EXISTS AdmSearchMemberByName;

CREATE PROCEDURE AdmSearchMemberByName ( param2 VARCHAR(255))
 BEGIN
		  SELECT
					users.id AS id,
					users.username AS username,
					CekRole(users.role_id) AS role,
					tbl_member.NIK AS NIK,
					tbl_member.user_id AS user_id,
					tbl_member.user_register_id AS user_register_id,
					tbl_member.complete_name AS complete_name,
					CekGender(tbl_member.jenis_kelamin) AS jenis_kelamin,
					tbl_member.gol_darah AS gol_darah,
					CekStatNikah(tbl_member.status_perkawinan) AS status_perkawinan,
					tbl_member.jenis_pekerjaan AS jenis_pekerjaan_id,
					CekKerja(tbl_member.jenis_pekerjaan) AS jenis_pekerjaan,
					tbl_member.telp AS telp,
					users.email AS email,
					CekMemberStatus(tbl_member.status) AS status
					
		 FROM users
		 JOIN tbl_member
					ON users.id = tbl_member.user_id

		WHERE users.username LIKE CONCAT('%',param2,'%');
				
		
 END;
//

DELIMITER ;

/*admin search user*/

/*----- admin delete user---*/
DELIMITER //

DROP PROCEDURE IF EXISTS AdmDeleteMember;

CREATE PROCEDURE AdmDeleteMember (IN ids INT(10), OUT flag VARCHAR(20) )
 BEGIN
		
		
		  DELETE 
				FROM users
			WHERE id = ids;
			
			DELETE 
				FROM tbl_member
			WHERE user_id = ids;
			
			DELETE 
				FROM club_member
			WHERE user_id = ids;
			
		SET flag = "Success";	
				
		
		
 END;
//

DELIMITER ;
/*---admin delete user---*/

/*--- admin club list -----*/
DELIMITER //

DROP PROCEDURE IF EXISTS AdmClubList;

CREATE PROCEDURE AdmClubList ()
 BEGIN
		
		
		   SELECT
					tbl_club.club_id AS id,
					tbl_club.nama_klub AS nama_klub,
					tbl_club.status AS status_id,
					CekKlubStatus(tbl_club.status) AS status
					
		 FROM tbl_club;	
		 
				
		
 END;
//

DELIMITER ;


/*end of admin club list ------*/

/*club update*/

DELIMITER //

DROP PROCEDURE IF EXISTS AdminUpdateClubStatus;

CREATE PROCEDURE AdminUpdateClubStatus (IN id_u INT, IN statuses VARCHAR(255), OUT flag VARCHAR(10))
 BEGIN
		
		START TRANSACTION;
		
		UPDATE tbl_club 
		SET
			status = statuses

		WHERE
				club_id = id_u; 

		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//

/*---- unit query ---*/

/*unit list*/

DELIMITER //

DROP PROCEDURE IF EXISTS AdmUnitList;

CREATE PROCEDURE AdmUnitList()
 BEGIN
		
		
		  SELECT
					tbl_unit.unit_id AS id,
					tbl_unit.user_id as user_id,
					tbl_unit.register_id as register_id ,
					tbl_unit.kaliber as kaliber,
					tbl_unit.unit_type as unit_type,
					tbl_unit.merek as merek,
					tbl_unit.mekanisme as mekanisme,
					tbl_unit.foto as foto,
					tbl_unit.status AS status_id,
					CekUnitStatus(tbl_unit.status) AS status,
					tbl_unit.created_at AS created_at,
					users.username AS pemilik
		 FROM tbl_unit
		 JOIN users
					ON tbl_unit.user_id = users.id
		 ORDER BY status_id ASC;
				
		
 END;
//

DELIMITER ;

/*unit by id*/

DELIMITER //

DROP PROCEDURE IF EXISTS AdmUnitById;

CREATE PROCEDURE AdmUnitById (unit_id INT)
 BEGIN
		
		
		  SELECT
					tbl_unit.unit_id AS id,
					tbl_unit.register_id as register_id,
					tbl_unit.kaliber AS kaliber,
					tbl_unit.unit_type AS unit_type,
					tbl_unit.merek AS merek,
					tbl_unit.foto AS foto,
					tbl_unit.mekanisme AS mekanisme,
					tbl_unit.status AS status_id,
					CekUnitStatus(tbl_unit.status) AS status,
					tbl_unit.created_at AS created_at,
					users.username AS pemilik
		 FROM tbl_unit
		 JOIN users 
					ON users.id = tbl_unit.user_id
			WHERE 
				tbl_unit.unit_id = unit_id;
				
		
 END;
//

DELIMITER ;

/*end of unit by id*/

/*club update*/

DELIMITER //

DROP PROCEDURE IF EXISTS AdmUpdateUnitReg;

CREATE PROCEDURE AdmUpdateUnitReg (IN id_u INT, IN reg_id VARCHAR(255), OUT flag VARCHAR(10))
 BEGIN
		
		START TRANSACTION;
		
		UPDATE tbl_unit 
		SET
			register_id = reg_id,
			status = 1

		WHERE
				unit_id = id_u; 

		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//

/*--- update status unit ----*/
DELIMITER //

DROP PROCEDURE IF EXISTS AdmUpdateUnitStat;

CREATE PROCEDURE AdmUpdateUnitStat (IN id_u INT, IN statuses VARCHAR(255), OUT flag VARCHAR(10))
 BEGIN
		
		START TRANSACTION;
		
		UPDATE tbl_unit 
		SET
			`status` = statuses

		WHERE
				unit_id = id_u; 

		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//
/*----- unit delete---*/
DELIMITER //

DROP PROCEDURE IF EXISTS AdmDeleteUnit;

CREATE PROCEDURE AdmDeleteUnit (IN ids INT(10), OUT flag VARCHAR(20) )
 BEGIN
		
		
		  DELETE 
				FROM tbl_unit
			WHERE unit_id = ids;
			
			
		SET flag = "Success";	
				
		
		
 END;
//

DELIMITER ;
/*---club delete end---*/
/*----- end of unit query ----*/
