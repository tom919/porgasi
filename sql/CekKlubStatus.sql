DELIMITER //

DROP FUNCTION IF EXISTS CekKlubStatus;

CREATE FUNCTION CekKlubStatus ( id INT )
RETURNS varchar(20) DETERMINISTIC

BEGIN

   DECLARE statklub varchar(20);

	 
	 CASE id
    
		WHEN 1 
		THEN SET statklub='Di Setujui';
    
		WHEN 2 
		THEN SET statklub ='Di Tolak';
		
		WHEN 3 
		THEN SET statklub ='Mohon Di Perbaiki';
    
		ELSE SET statklub ='Menunggu Pemeriksaan';
		
		END CASE;

   RETURN statklub;

END; //

DELIMITER ;