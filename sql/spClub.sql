
DELIMITER //

DROP PROCEDURE IF EXISTS InsertClub;

CREATE PROCEDURE InsertClub (IN nama_klub VARCHAR(255), IN alamat_klub TEXT, IN ketua INT(10), IN sekretaris INT(10), IN bendahara INT(10), IN creator INT(10), OUT flag VARCHAR(10))
 BEGIN
		
		START TRANSACTION;
		
		INSERT INTO tbl_club 
		(
			nama_klub,
			alamat_klub,
			tgl_daftar,
			ketua,
			sekretaris,
			bendahara,
			status,
			created_by
		) 
		VALUES 
		( 
			nama_klub, 
			alamat_klub, 
			NOW(), 
			ketua, 
			sekretaris,
			bendahara,
			0,
			creator
		);
		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//

DELIMITER ;

/*insert member club*/

DELIMITER //

DROP PROCEDURE IF EXISTS InsertMemberClub;

CREATE PROCEDURE InsertMemberClub (IN club_id INT(10), IN user_id INT(10), OUT flag VARCHAR(10))
 BEGIN
		
		START TRANSACTION;
		
		INSERT INTO club_member 
		(
			club_id,
			user_id,
			tanggal_daftar,
			status
		) 
		VALUES 
		( 
			club_id, 
			user_id, 
			NOW(), 
			1
		);
		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//

DELIMITER ;

/*end of insert member club*/

/*club list*/

DELIMITER //

DROP PROCEDURE IF EXISTS ClubList;

CREATE PROCEDURE ClubList (id INT)
 BEGIN
		
		
		  SELECT
					tbl_club.club_id AS id,
					tbl_club.nama_klub AS nama_klub,
					tbl_club.status AS status_id,
					CekKlubStatus(tbl_club.status) AS status
					
		 FROM tbl_club
			WHERE tbl_club.created_by = id;			
		
 END;
//

DELIMITER ;

/*end of club list*/

/*club detail*/

DELIMITER //

DROP PROCEDURE IF EXISTS ClubDetail;

CREATE PROCEDURE ClubDetail (id INT)
 BEGIN
		
		
		  SELECT
					tbl_club.club_id AS id,
					tbl_club.nama_klub AS nama_klub,
					tbl_club.alamat_klub AS alamat,
					tbl_club.ketua AS id_ketua,
					p1.username AS username_ketua, 
					tbl_club.sekretaris AS id_sekretaris,
					p2.username AS username_sekretaris,
					tbl_club.bendahara AS id_bendahara,
					p3.username AS username_bendahara,
					tbl_club.status AS status_id,
					CekKlubStatus(tbl_club.status) AS status
					
		 FROM tbl_club
				LEFT JOIN users AS p1 ON 
					tbl_club.ketua = p1.id
				LEFT JOIN users AS p2 ON 
					tbl_club.sekretaris = p2.id
				LEFT JOIN users AS p3 ON 
					tbl_club.bendahara = p3.id 
			WHERE tbl_club.club_id = id
			LIMIT 1;
		
 END;
//

DELIMITER ;

/*club detail*/


/*club update*/

DELIMITER //

DROP PROCEDURE IF EXISTS UpdateClub;

CREATE PROCEDURE UpdateClub (IN id_u INT, IN nama_klub_u VARCHAR(255), IN alamat_klub_u TEXT, IN ketua_u INT(10), IN sekretaris_u INT(10), IN bendahara_u INT(10), OUT flag VARCHAR(10))
 BEGIN
		
		START TRANSACTION;
		
		UPDATE tbl_club 
		SET
			nama_klub = nama_klub_u,
			alamat_klub = alamat_klub_u,
			ketua = ketua_u,
			sekretaris = sekretaris_u,
			bendahara = bendahara_u

		WHERE
				club_id = id_u; 

		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//

DELIMITER ;
/* end of club update */

/*----- club delete---*/
DELIMITER //

DROP PROCEDURE IF EXISTS DeleteClub;

CREATE PROCEDURE DeleteClub (IN ids INT(10), OUT flag VARCHAR(20) )
 BEGIN
		
		
		  DELETE 
				FROM tbl_club
			WHERE club_id = ids;
			
			DELETE 
				FROM club_member
			WHERE club_id = ids;
			
		SET flag = "Success";	
				
		
		
 END;
//

DELIMITER ;
/*---club delete end---*/

/*member of club list*/

DELIMITER //

DROP PROCEDURE IF EXISTS MemberClubList;

CREATE PROCEDURE MemberClubList (id INT)
 BEGIN
		
		
		  SELECT
					club_member.id AS id,
					club_member.club_id AS club_id,
					club_member.user_id AS user_id,
					users.username AS username,
					CekMemberStatus(users.flag) AS status_member
										
		 FROM club_member
			JOIN users
			ON users.id = club_member.user_id
			WHERE club_member.club_id = id;			
		
 END;
//

DELIMITER ;

/*end of member of club list*/

/*----- member of club delete---*/
DELIMITER //

DROP PROCEDURE IF EXISTS DeleteClubMember;

CREATE PROCEDURE DeleteClubMember (IN ids INT(10), OUT flag VARCHAR(20) )
 BEGIN
			
			DELETE 
				FROM club_member
			WHERE id = ids;
			
		SET flag = "Success";	
				
		
		
 END;
//

DELIMITER ;
/*---member of club delete end---*/
