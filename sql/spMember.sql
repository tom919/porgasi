DELIMITER //

DROP PROCEDURE IF EXISTS GetMemberProfile;

CREATE PROCEDURE GetMemberProfile (id INT)
 BEGIN
  SELECT
		users.username AS username,
		CekRole(users.role_id) AS role,
		tbl_member.NIK AS NIK,
		tbl_member.user_id AS user_id,
		tbl_member.user_register_id AS user_register_id,
		tbl_member.complete_name AS complete_name,
		tbl_member.gol_darah AS gol_darah,
		tbl_member.jenis_kelamin AS jenis_kelamin_id,
		CekGender(tbl_member.jenis_kelamin) AS jenis_kelamin,
		tbl_member.tempat_lahir AS tempat_lahir,
		tbl_member.tanggal_lahir AS tanggal_lahir,
		tbl_member.nama_ayah AS nama_ayah,
		tbl_member.nama_ibu AS nama_ibu,
		tbl_member.nama_pasangan AS nama_pasangan,
		tbl_member.status_perkawinan AS status_perkawinan_id,
		CekStatNikah(tbl_member.status_perkawinan) AS status_perkawinan,
		tbl_member.alamat AS alamat,
		tbl_member.provinsi AS provinsi,
		tbl_member.kabupatenkota AS kabupatenkota,
		tbl_member.kecamatan AS kecamatan,
		tbl_member.kelurahan AS kelurahan,
		tbl_member.kode_pos AS kode_pos,
		tbl_member.jenis_pekerjaan AS jenis_pekerjaan_id,
		CekKerja(tbl_member.jenis_pekerjaan) AS jenis_pekerjaan,
		tbl_member.nama_kantor AS nama_kantor,
		tbl_member.alamat_kantor AS alamat_kantor,
		tbl_member.telp AS telp,
		users.email AS email,
		tbl_member.pendidikan_sd AS pendidikan_sd,
		tbl_member.thn_pendidikan_sd AS thn_pendidikan_sd,
		tbl_member.pendidikan_smp AS pendidikan_smp,
		tbl_member.thn_pendidikan_smp AS thn_pendidikan_smp,
		tbl_member.pendidikan_sma AS pendidikan_sma,
		tbl_member.thn_pendidikan_sma AS thn_pendidikan_sma,
		tbl_member.pendidikan_sarjana AS pendidikan_sarjana,
		tbl_member.thn_pendidikan_sarjana AS thn_pendidikan_sarjana,
		tbl_member.pengalaman_kerja1 AS pengalaman_kerja1,
		tbl_member.thn_pengalaman_kerja1 AS thn_pengalaman_kerja1,
		tbl_member.pengalaman_kerja2 AS pengalaman_kerja2,
		tbl_member.thn_pengalaman_kerja2 AS thn_pengalaman_kerja2,
		tbl_member.pengalaman_kerja3 AS pengalaman_kerja3,
		tbl_member.thn_pengalaman_kerja3 AS thn_pengalaman_kerja3,
		tbl_member.pengalaman_kerja4 AS pengalaman_kerja4,
		tbl_member.thn_pengalaman_kerja4 AS thn_pengalaman_kerja4,
		tbl_member.status AS status_id,
		CekMemberStatus(tbl_member.status) AS status
 FROM tbl_member
 JOIN users 
			ON users.id = tbl_member.user_id
  WHERE tbl_member.user_id = id;
 END;
//

DELIMITER ;


DELIMITER //

DROP PROCEDURE IF EXISTS GetMemberCheck;

CREATE PROCEDURE GetMemberCheck (id INT)
 BEGIN
  SELECT
		COUNT(*) AS counter
 FROM tbl_member
 JOIN users 
			ON users.id = tbl_member.user_id
  WHERE tbl_member.user_id = id;
 END;
//

DELIMITER ;