DELIMITER //

DROP FUNCTION IF EXISTS CekMemberStatus;

CREATE FUNCTION CekMemberStatus ( id INT )
RETURNS varchar(30) DETERMINISTIC

BEGIN

   DECLARE StatMember varchar(20);

   IF id = 0 THEN
      SET StatMember = 'Menunggu Pemeriksaan';

   ELSEIF id = 1 THEN
      SET StatMember = 'Aktif';
			
	 ELSEIF id = 2 THEN
      SET StatMember = 'Suspend';

	  ELSEIF id = 3 THEN
      SET StatMember = 'Terblokir';
			
		ELSEIF id = 4 THEN
      SET StatMember = 'Data Tidak Lengkap';
   
	 ELSEIF id = 5 THEN
      SET StatMember = 'Data Terindikasi Palsu';
	 
	 ELSE
      SET StatMember = 'Unknown';

   END IF;

   RETURN StatMember;

END; //

DELIMITER ;