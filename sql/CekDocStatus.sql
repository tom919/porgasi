DELIMITER //

DROP FUNCTION IF EXISTS CekDocStatus;

CREATE FUNCTION CekDocStatus ( id INT )
RETURNS varchar(20) DETERMINISTIC

BEGIN

   DECLARE statdokumen varchar(20);

	 
	 CASE id
	 
	 	WHEN 0 
		THEN SET statdokumen ='Menunggu Pemeriksaan';
    
		WHEN 1 
		THEN SET statdokumen ='Di Setujui';
    
		WHEN 2 
		THEN SET statdokumen ='Di Tolak';
		
		WHEN 3 
		THEN SET statdokumen ='Mohon Di Perbaiki';
		
		WHEN 4 
		THEN SET statdokumen ='Terindikasi Palsu';
    
		ELSE SET statdokumen ='Tidak Diketahui';
		
		END CASE;

   RETURN statdokumen;

END; //

DELIMITER ;