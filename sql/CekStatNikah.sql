DELIMITER //

DROP FUNCTION IF EXISTS CekStatNikah;

CREATE FUNCTION CekStatNikah ( id INT )
RETURNS varchar(30) DETERMINISTIC

BEGIN

   DECLARE StatNikah varchar(20);

   IF id = 0 THEN
      SET StatNikah = 'Belum Menikah';

   ELSEIF id = 1 THEN
      SET StatNikah = 'Menikah';

   ELSE
      SET StatNikah = 'Cerai';

   END IF;

   RETURN StatNikah;

END; //

DELIMITER ;