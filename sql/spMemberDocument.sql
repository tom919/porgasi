DELIMITER //

DROP PROCEDURE IF EXISTS InsertMemberDocument;

CREATE PROCEDURE InsertMemberDocument (IN user_id INTEGER(8), IN category VARCHAR(255), IN document_name VARCHAR(255), IN status VARCHAR(20), OUT flag VARCHAR(10))
 BEGIN
		
		START TRANSACTION;
		
		INSERT INTO tbl_document (user_id,category,document_name,status, created_at) VALUES ( user_id, category, document_name, status, NOW());
		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//

DELIMITER ;

/*document list*/

DELIMITER //

DROP PROCEDURE IF EXISTS MemberDocumentList;

CREATE PROCEDURE MemberDocumentList (id INT)
 BEGIN
		
		
		  SELECT
					tbl_document.id AS id,
					tbl_document.user_id as uid,
					CekDocCategory(tbl_document.category) AS category ,
					tbl_document.category AS category_id,
					tbl_document.document_name AS document_name,
					tbl_document.status AS status_id,
					CekDocStatus(tbl_document.status) AS status,
					tbl_document.created_at AS created_at
		 FROM tbl_document
		 JOIN users 
					ON users.id = tbl_document.user_id
			WHERE users.id = id;
				
		
 END;
//

DELIMITER ;

/*end of document list*/

/*document by id*/

DELIMITER //

DROP PROCEDURE IF EXISTS MemberDocumentById;

CREATE PROCEDURE MemberDocumentById (id INT, dok_id INT)
 BEGIN
		
		
		  SELECT
					tbl_document.id AS id,
					CekDocCategory(tbl_document.category) AS category ,
					tbl_document.category AS category_id,
					tbl_document.document_name AS document_name,
					tbl_document.status AS status_id,
					CekDocStatus(tbl_document.status) AS status,
					tbl_document.created_at AS created_at
		 FROM tbl_document
		 JOIN users 
					ON users.id = tbl_document.user_id
			WHERE users.id = id
				AND
				tbl_document.id = dok_id;
				
		
 END;
//

DELIMITER ;

/*end of document by id*/

/*------document delete---*/
DELIMITER //

DROP PROCEDURE IF EXISTS MemberDocumentDelete;

CREATE PROCEDURE MemberDocumentDelete (IN ids INT(10), OUT flag VARCHAR(20) )
 BEGIN
		
		
		  DELETE 
				FROM tbl_document
			WHERE id = ids;
			
		SET flag = "Success";	
				
		
		
 END;
//

DELIMITER ;
/*---document delete end---*/

