DELIMITER //

DROP FUNCTION IF EXISTS CekDocCategory;

CREATE FUNCTION CekDocCategory ( id INT )
RETURNS varchar(20) DETERMINISTIC

BEGIN

   DECLARE jenisdokumen varchar(20);

	 
	 CASE id
    WHEN 1 
		THEN SET jenisdokumen ='KTP';
    
		WHEN 2 
		THEN SET jenisdokumen ='SIM';
    
		WHEN 3 
		THEN SET jenisdokumen ='SKCK';
		
		WHEN 4 
		THEN SET jenisdokumen ='Kartu Keluarga';
		
		WHEN 5 
		THEN SET jenisdokumen ='Surat Rekomendasi';
		
		WHEN 6 
		THEN SET jenisdokumen ='Foto';
		
		WHEN 7 
		THEN SET jenisdokumen ='Lain-lain';
    
		ELSE SET jenisdokumen ='Jenis Tidak Dikenali';
		
		END CASE;

   RETURN jenisdokumen;

END; //

DELIMITER ;