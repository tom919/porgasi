DELIMITER //

DROP FUNCTION IF EXISTS CekRole;

CREATE FUNCTION CekRole ( id INT )
RETURNS varchar(30) DETERMINISTIC

BEGIN

   DECLARE Role varchar(20);

   IF id = 0 THEN
      SET Role = 'Super Admin';

   ELSEIF id = 1 THEN
      SET Role = 'Admin';
			
	 ELSEIF id = 2 THEN
      SET Role = 'Polisi';

	  ELSEIF id = 3 THEN
      SET Role = 'Member';
   
	 ELSE
      SET Role = 'Unknown';

   END IF;

   RETURN Role;

END; //

DELIMITER ;