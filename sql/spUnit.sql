DELIMITER //

DROP PROCEDURE IF EXISTS InsertUnit;

CREATE PROCEDURE InsertUnit (IN user_id INTEGER(11), IN kaliber VARCHAR(50), IN unit_type VARCHAR(50), IN merek VARCHAR(255), IN mekanisme VARCHAR(255), IN foto VARCHAR(255), IN status VARCHAR(20), OUT flag VARCHAR(10))
 BEGIN
		
		START TRANSACTION;
		
		INSERT INTO tbl_unit (user_id,kaliber,unit_type, merek, mekanisme, foto, status, created_at) VALUES ( user_id,kaliber,unit_type, merek, mekanisme, foto, status, NOW());
		
		SET flag = "success";
		
		COMMIT;
		
		
		
 END;
//

DELIMITER ;

/*unit list*/

DELIMITER //

DROP PROCEDURE IF EXISTS UnitList;

CREATE PROCEDURE UnitList (id INT)
 BEGIN
		
		
		  SELECT
					tbl_unit.unit_id AS id,
					tbl_unit.user_id as user_id,
					tbl_unit.register_id as register_id ,
					tbl_unit.kaliber as kaliber,
					tbl_unit.unit_type as unit_type,
					tbl_unit.merek as merek,
					tbl_unit.mekanisme as mekanisme,
					tbl_unit.foto as foto,
					tbl_unit.status AS status_id,
					CekUnitStatus(tbl_unit.status) AS status,
					tbl_unit.created_at AS created_at
		 FROM tbl_unit
		 JOIN users 
					ON users.id = tbl_unit.user_id
			WHERE users.id = id;
				
		
 END;
//

DELIMITER ;

/*unit by id*/

DELIMITER //

DROP PROCEDURE IF EXISTS UnitById;

CREATE PROCEDURE UnitById (id INT, unit_id INT)
 BEGIN
		
		
		  SELECT
					tbl_unit.unit_id AS id,
					tbl_unit.kaliber AS kaliber,
					tbl_unit.unit_type AS unit_type,
					tbl_unit.merek AS merek,
					tbl_unit.foto AS foto,
					tbl_unit.mekanisme AS mekanisme,
					tbl_unit.status AS status_id,
					tbl_unit.created_at AS created_at
		 FROM tbl_unit
		 JOIN users 
					ON users.id = tbl_unit.user_id
			WHERE users.id = id
				AND
				tbl_unit.unit_id = unit_id;
				
		
 END;
//

DELIMITER ;

/*end of unit by id*/

/*------ unit delete---*/
DELIMITER //

DROP PROCEDURE IF EXISTS UnitDelete;

CREATE PROCEDURE UnitDelete (IN ids INT(10), OUT flag VARCHAR(20) )
 BEGIN
		
		
		  DELETE 
				FROM tbl_unit
			WHERE unit_id = ids;
			
			
		SET flag = "Success";	
				
		
		
 END;
//

DELIMITER ;
/*--- unit delete end---*/