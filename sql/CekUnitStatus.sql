DELIMITER //

DROP FUNCTION IF EXISTS CekUnitStatus;

CREATE FUNCTION CekUnitStatus ( id INT )
RETURNS varchar(100) DETERMINISTIC

BEGIN

   DECLARE statunit varchar(100);

	 
	 CASE id
	 
	 	WHEN 0 
		THEN SET statunit ='Menunggu Pemeriksaan';
    
		WHEN 1 
		THEN SET statunit ='Aktif';
    
		WHEN 2 
		THEN SET statunit ='Proses Pemusnahan';
		
		WHEN 3 
		THEN SET statunit ='Proses Transfer Pemilik';
		
		WHEN 4 
		THEN SET statunit ='Data Salah';
    
		ELSE SET statunit ='Tidak Diketahui';
		
		END CASE;

   RETURN statunit;

END; //

DELIMITER ;