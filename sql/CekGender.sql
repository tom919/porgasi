DELIMITER //

DROP FUNCTION IF EXISTS CekGender;

CREATE FUNCTION CekGender ( id INT )
RETURNS varchar(20) DETERMINISTIC

BEGIN

   DECLARE jeniskelamin varchar(20);

   IF id = 1 THEN
      SET jeniskelamin = 'Laki - laki';

   ELSEIF id = 2 THEN
      SET jeniskelamin = 'Perempuan';

   ELSE
      SET jeniskelamin = 'Alien';

   END IF;

   RETURN jeniskelamin;

END; //

DELIMITER ;